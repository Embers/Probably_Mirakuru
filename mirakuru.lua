--[[
	Mirakuru Profiles - Core settings
	Created by Mirakuru
	
	Released under the GNU GENERAL PUBLIC LICENSE V2
	
	Description:
		Contains all the generic settings and functions required
		to operate Mirakuru's combat routines.
]]

---[[ General configuration & settings ]]
-- Library table
mirakuru = {}
ProbablyEngine.library.register("mirakuru", mirakuru)

-- Auspicious Spirits tracker
local SA_TOTAL = SA_TOTAL or 0
local SA_STATS = SA_STATS or {}

-- Spells with travel time
local spellTravelling = false
local spellsWithTravelTime = {[48181] = true, [6353] = true, [105174] = true, [86040] = true}

-- Codex of Xerrath for Warlocks
moltenCore = 122355
if IsPlayerSpell(137206) then moltenCore = 140074 end
if IsPlayerSpell(101508) then moltenCore = 140074 end

-- Affliction Warlock Soulshard Regeneration Timer
local ssTimer = 0

-- Buff Tables
local intBuffs = {[177594] = true, [126683] = true, [126705] = true}
local critBuffs = {[162919] = true, [177046] = true, [159234] = true}
local hasteBuffs = {[177051] = true, [176875] = true, [159675] = true, [176980] = true, [176882] = true, [165531] = true, [90355] = true, [2825] = true, [80353] = true, [32182] = true}
local masteryBuffs = {[176941] = true, [173322] = true}
local multistrikeBuffs = {[159676] = true, [177081] = true, [165832] = true}
local versatilityBuffs = {[165543] = true, [165833] = true}

-- Boss interrupts
local bossInterruptTimer = nil
local bossInterrupting = {[158102] = true, [162256] = true, [162259] = true}

-- Buff counts & timers
local msTimer = 0
local msCount = 0
local intTimer = 0
local intCount = 0
local critTimer = 0
local critCount = 0
local hasteTimer = 0
local hasteCount = 0
local versaTimer = 0
local versaCount = 0
local masteryTimer = 0
local masteryCount = 0

-- Multi-dotting trackers
doom = 0
agony = 0
immolate = 0
corruption = 0
vampiricTouch = 0
shadowWordPain = 0
unstableAffliction = 0

-- Tier Set Bonus
local tier17set = 0

-- Opener Tracker
mirakuru.opener = false

-- Tester function
function checkTable(table)
	local DiesalGUI = LibStub("DiesalGUI-1.0")
	local explore = DiesalGUI:Create('TableExplorer')
	explore:SetTable("Table Checker", table)
	explore:BuildTree()
end

--[[ Routine functions ]]
-- Routine commands
ProbablyEngine.command.register('miracle', function(msg, box)
	local command, text = msg:match("^(%S*)%s*(.-)$")
	if command == "config" then
		if GetSpecializationInfo(GetSpecialization()) == 265 then
			mirakuru.displayFrame(mira_affli)
		elseif GetSpecializationInfo(GetSpecialization()) == 266 then
			mirakuru.displayFrame(mira_demon)
		elseif GetSpecializationInfo(GetSpecialization()) == 267 then
			mirakuru.displayFrame(mira_destru)
		elseif GetSpecializationInfo(GetSpecialization()) == 258 then
			mirakuru.displayFrame()
		end
	end
	
	if command == "donate" then
		print("[|cff69CFF0Mirakuru's Profiles]|r For donations, use this link:")
		print("[|cff69CFF0Mirakuru's Profiles]|r http://goo.gl/ifxzlG")
		print("[|cff69CFF0Mirakuru's Profiles]|r")
		print("[|cff69CFF0Mirakuru's Profiles]|r Should you not want to donate, you can also support")
		print("[|cff69CFF0Mirakuru's Profiles]|r me directly by creating an account at OwnedCore and")
		print("[|cff69CFF0Mirakuru's Profiles]|r click the +rep button on me there!")
		print("[|cff69CFF0Mirakuru's Profiles]|r http://goo.gl/devH3k")
		print("[|cff69CFF0Mirakuru's Profiles]|r")
		print("[|cff69CFF0Mirakuru's Profiles]|r Thank you for using my routines!")
	end
	
	if command == "" then
		print("[|cff69CFF0Mirakuru's Profiles|r] Currently, only these commands are supported:")
		print("[|cff69CFF0Mirakuru's Profiles] /miracle config|r Opens your current spec's GUI")
		print("[|cff69CFF0Mirakuru's Profiles] /miracle donate|r Shows donation information")
	end
end)

-- Interface Frame
function mirakuru.displayFrame(frame)
	if not createFrame then
		windowRef = ProbablyEngine.interface.buildGUI(frame)
		createFrame = true
		frameState = true
		windowRef.parent:SetEventListener('OnClose', function()
			createFrame = false
			frameState = false
		end)
	elseif createFrame == true and frameState == true then
		windowRef.parent:Hide()
		frameState = false
	elseif createFrame == true and frameState == false then
		windowRef.parent:Show()
		frameState = true
	end
end

-- Round any number
function mirakuru.round(num, idp)
	return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end

-- Crowd Controls
function mirakuru.CC(unit)
	local CC = {118,28272,28271,61305,61721,61780,9484,3355,19386,339,6770,6358,20066,51514,115078,115268}
	for i=1,#CC do
		if UnitDebuff(unit,GetSpellInfo(CC[i])) then return true end
	end
	return false
end

--[[ Simple frontal check
function mirakuru.infront(unit)
	if not FireHack then return false end
	
	local aX, aY, aZ = ObjectPosition(unit)
	local bX, bY, bZ = ObjectPosition('player')
	local playerFacing = GetPlayerFacing()
	local facing = math.atan2(bY - aY, bX - aX) % 6.2831853071796
	return math.abs(math.deg(math.abs(playerFacing - (facing)))-180) < 90
end]]

--[[ Event listener ]]
-- Unit storage
tempUnitList = {}

-- Player entering combat
ProbablyEngine.listener.register("PLAYER_REGEN_DISABLED", function(...)
	local specID = GetSpecializationInfo(GetSpecialization())
	
	-- Sort counters by spec
	if specID == 258 then	-- Shadow Priest
		vampiricTouch = 0
		shadowWordPain = 0
	end
	if specID == 265 then	-- Affliction Warlock
		agony = 0
		corruption = 0
		unstableAffliction = 0
	end
	if specID == 266 then	-- Demonology Warlock
		doom = 0
		corruption = 0
	end
	if specID == 267 then	-- Destruction Warlock
		immolate = 0
	end
	
	-- Wipe Auspicious Spirits tracker
	SA_TOTAL = 0
	wipe(SA_STATS)
	
	-- Detect if we started an encounter
	if IsEncounterInProgress() then mirakuru.opener = false else
		if UnitExists("target") then
			local bossID = mirakuru.BossIDs
			local mobID = select(6,strsplit("-", UnitGUID("target")))
			
			-- Convert to number
			if type(mobID) ~= "number" then mobID = tonumber(mobID) end
			
			if UnitLevel("target") ~= -1 then
				if bossID[mobID] ~= nil then mirakuru.opener = false else mirakuru.opener = true end
			else mirakuru.opener = false end
		end
	end
	
	-- Reset interruption timer
	bossInterruptTimer = nil
	-- Reset Soulshard Timer
	ssTimer = 0
	-- Clear temporary unit table
	wipe(tempUnitList)
end)

-- Player leaving combat
ProbablyEngine.listener.register("PLAYER_REGEN_ENABLED", function(...)
	local specID = GetSpecializationInfo(GetSpecialization())
	
	-- Sort counters by spec
	if specID == 258 then	-- Shadow Priest
		vampiricTouch = 0
		shadowWordPain = 0
	end
	if specID == 265 then	-- Affliction Warlock
		agony = 0
		corruption = 0
		unstableAffliction = 0
	end
	if specID == 266 then	-- Demonology Warlock
		doom = 0
		corruption = 0
	end
	if specID == 267 then	-- Destruction Warlock
		immolate = 0
	end
	
	-- Wipe Auspicious Spirits tracker
	SA_TOTAL = 0
	wipe(SA_STATS)
	
	-- Reset interruption timer
	bossInterruptTimer = nil
	-- Reset Soulshard Timer
	ssTimer = 0
	-- Reset Opener
	mirakuru.opener = false
	-- Clear temporary unit table
	wipe(tempUnitList)
end)

-- Events occurring after a reload/port/bg/etc
ProbablyEngine.listener.register("PLAYER_ENTERING_WORLD", function(...)
	local specID = GetSpecializationInfo(GetSpecialization())
	
	-- Reset while checking
	if tier17 ~= 0 then tier17 = 0 end
	
	-- Warlock Tier 17
	if specID == 267 then
		local tier17 = {115585,115586,115587,115588,115589}
		
		for i=1,#tier17 do
			if IsEquippedItem(tier17[i]) then tier17set = tier17set + 1 end
		end
	end
	
	-- Shadow Tier 17
	if specID == 258 then
		local tier17 = {115560,115561,115562,115563,115564}
		
		for i=1,#tier17 do
			if IsEquippedItem(tier17[i]) then tier17set = tier17set + 1 end
		end
	end
end)


-- Combat Log events
ProbablyEngine.listener.register("COMBAT_LOG_EVENT_UNFILTERED", function(...)
	local event			= select(2, ...)
	local sourceGUID	= select(4, ...)
	local targetGUID	= select(8, ...)
	local spellID		= select(12, ...)
	local player		= UnitGUID("player")
	local specID = GetSpecializationInfo(GetSpecialization())
	
	-- Death events
	if event == "UNIT_DIED" or event == "UNIT_DESTROYED" or event == "SPELL_INSTAKILL" then
		if tempUnitList[targetGUID] ~= nil then
			if specID == 267 then	-- Destruction Warlock
				if tempUnitList[targetGUID].immo then immolate = immolate - 1 end
			end
			if specID == 265 then	-- Affliction Warlock
				if tempUnitList[targetGUID].ag then agony = agony - 1 end
				if tempUnitList[targetGUID].cor then corruption = corruption - 1 end
				if tempUnitList[targetGUID].ua then unstableAffliction = unstableAffliction - 1 end
			end
			if specID == 266 then	-- Demonology Warlock
				if tempUnitList[targetGUID].doom then doom = doom - 1 end
				if tempUnitList[targetGUID].cor then corruption = corruption - 1 end
			end
			if specID == 258 then	-- Shadow Priest
				if tempUnitList[targetGUID].vt then vampiricTouch = vampiricTouch - 1 end
				if tempUnitList[targetGUID].swp then shadowWordPain = shadowWordPain - 1 end
			end
			
			-- This little unit went to sleep...
			tempUnitList[targetGUID] = nil
		end
		
		-- Auspicious Spirits tracker
		if SA_STATS[targetGUID] then
			SA_TOTAL = SA_TOTAL - SA_STATS[targetGUID].count
			if SA_TOTAL < 0 then SA_TOTAL = 0 end
			SA_STATS[targetGUID].count = nil
			SA_STATS[targetGUID].timer = nil
			SA_STATS[targetGUID] = nil
		end
	end
	
	-- Buff and Proc tracker
	if targetGUID == player then
		-- Auras applied to you
		if event == "SPELL_AURA_APPLIED" then
			-- Detect +stat buffs
			if intBuffs[spellID] then intCount = intCount + 1 intTimer = select(7,UnitBuff("player", GetSpellInfo(spellID))) end
			if critBuffs[spellID] then critCount = critCount + 1 critTimer = select(7,UnitBuff("player", GetSpellInfo(spellID))) end
			if hasteBuffs[spellID] then hasteCount = hasteCount + 1 hasteTimer = select(7,UnitBuff("player", GetSpellInfo(spellID))) end
			if masteryBuffs[spellID] then masteryCount = masteryCount + 1 masteryTimer = select(7,UnitBuff("player", GetSpellInfo(spellID))) end
			if multistrikeBuffs[spellID] then msCount = msCount + 1 msTimer = select(7,UnitBuff("player", GetSpellInfo(spellID))) end
			if versatilityBuffs[spellID] then versaCount = versaCount + 1 versaTimer = select(7,UnitBuff("player", GetSpellInfo(spellID))) end
		end
		
		-- Aura Removed
		if event == "SPELL_AURA_REMOVED" then
			-- Detect +stat buffs
			if intBuffs[spellID] then intCount = intCount - 1 end
			if critBuffs[spellID] then critCount = critCount - 1 end
			if hasteBuffs[spellID] then hasteCount = hasteCount - 1 end
			if masteryBuffs[spellID] then masteryCount = masteryCount - 1 end
			if multistrikeBuffs[spellID] then msCount = msCount - 1 end
			if versatilityBuffs[spellID] then versaCount = versaCount - 1 end
		end
	end
	
	-- Filter all events to those originating from ourself
	if sourceGUID == player then
		-- Spell casting events
		if event == "SPELL_CAST_START" then
			-- Detect spells with a travelling time that applies a debuff/dot
			if spellsWithTravelTime[spellID] then spellTravelling = true end
			
			-- Soul Fire without Molten Core procs
			if UnitAffectingCombat("player") then
				if spellID == 6353 and not UnitBuff("player", GetSpellInfo(moltenCore)) then SpellStopCasting() return true end
			end
		end
		
		-- Casting Successfull
		if event == "SPELL_CAST_SUCCESS" then
			if spellID == 48181 or spellID == 74434 then ssTimer = GetTime() end
			
			-- Auspicious Spirits tracker
			if spellID == 147193 then	-- Shadowy Apparition spawned
				if not SA_STATS[targetGUID] or SA_STATS[targetGUID] == nil then
					SA_STATS[targetGUID] = {}
					SA_STATS[targetGUID].count = 0
				end
				SA_TOTAL = SA_TOTAL + 1
				SA_STATS[targetGUID].count = SA_STATS[targetGUID].count + 1
				SA_STATS[targetGUID].timer = GetTime()
			end
			if spellID == 155521 then	-- Auspicious Spirit hit
				SA_TOTAL = SA_TOTAL - 1
				if SA_STATS[targetGUID] and SA_STATS[targetGUID].count > 0 then
					SA_STATS[targetGUID].count = SA_STATS[targetGUID].count - 1
					SA_STATS[targetGUID].timer = GetTime()
					if SA_STATS[targetGUID].count <= 0 then
						SA_STATS[targetGUID].count = nil
						SA_STATS[targetGUID].timer = nil
						SA_STATS[targetGUID] = nil
					end
				end
			end
		end
		
		-- Casting interrupted
		if event == "SPELL_CAST_FAILED" then
			-- Detect spells with a travelling time that applies a debuff/dot
			if spellsWithTravelTime[spellID] then spellTravelling = false end
		end
		
		-- Spell damage
		if event == "SPELL_DAMAGE" then
			-- Detect spells with a travelling time that dealt direct damage
			if spellsWithTravelTime[spellID] then spellTravelling = false end
		end
		
		-- Aura applied by you refreshed
		if event == "SPELL_AURA_REFRESH" then
			-- Detect spells with a travelling time that applies a debuff/dot
			if spellsWithTravelTime[spellID] then spellTravelling = false end
		end
		
		-- Auras applied by you
		if event == "SPELL_AURA_APPLIED" then
			-- Detect spells with a travelling time that applies a debuff/dot
			if spellsWithTravelTime[spellID] then spellTravelling = false end
			
			-- Track debuffs
			if specID == 267 then	-- Destruction Warlock
				if spellID == 157736 then	-- Immolate
					if not tempUnitList[targetGUID] then
						tempUnitList[targetGUID] = {immo = true}
					else tempUnitList[targetGUID].immo = true end
					immolate = immolate + 1
				end
			end
			if specID == 265 then	-- Affliction Warlock
				if spellID == 980 then		-- Agony
					if not tempUnitList[targetGUID] then
						tempUnitList[targetGUID] = {ag = true}
					else tempUnitList[targetGUID].ag = true end
					agony = agony + 1
				end
				if spellID == 146739 then	-- Corruption
					if not tempUnitList[targetGUID] then
						tempUnitList[targetGUID] = {cor = true}
					else tempUnitList[targetGUID].cor = true end
					corruption = corruption + 1
				end
				if spellID == 30108 then	-- Unstable Affliction
					if not tempUnitList[targetGUID] then
						tempUnitList[targetGUID] = {ua = true}
					else tempUnitList[targetGUID].ua = true end
					unstableAffliction = unstableAffliction + 1
				end
			end
			if specID == 266 then	-- Demonology Warlock
				if spellID == 146739 then	-- Corruption
					if not tempUnitList[targetGUID] then
						tempUnitList[targetGUID] = {cor = true}
					else tempUnitList[targetGUID].cor = true end
					corruption = corruption + 1
				end
				if spellID == 603 then	-- Doom
					if not tempUnitList[targetGUID] then
						tempUnitList[targetGUID] = {doom = true}
					else tempUnitList[targetGUID].doom = true end
					doom = doom + 1
				end
			end
			if specID == 258 then	-- Shadow Priest
				if spellID == 589 then		-- Shadow Word: Pain
					if not tempUnitList[targetGUID] then
						tempUnitList[targetGUID] = {swp = true}
					else tempUnitList[targetGUID].swp = true end
					shadowWordPain = shadowWordPain + 1
				end
				if spellID == 34914 then	-- Vampiric Touch
					if not tempUnitList[targetGUID] then
						tempUnitList[targetGUID] = {vt = true}
					else tempUnitList[targetGUID].vt = true end
					vampiricTouch = vampiricTouch + 1
				end
			end
		end
		
		-- Auras applied by you faded
		if event == "SPELL_AURA_REMOVED" then
			-- Track debuffs
			if tempUnitList[targetGUID] ~= nil then
				if specID == 267 then	-- Destruction Warlock
					if spellID == 157736 then	-- Immolate
						immolate = immolate - 1
						tempUnitList[targetGUID].immo = false
					end
				end
				if specID == 265 then	-- Affliction Warlock
					if spellID == 980 then		-- Agony
						agony = agony - 1
						tempUnitList[targetGUID].ag = false
					end
					if spellID == 146739 then	-- Corruption
						corruption = corruption - 1
						tempUnitList[targetGUID].cor = false
					end
					if spellID == 30108 then	-- Unstable Affliction
						unstableAffliction = unstableAffliction - 1
						tempUnitList[targetGUID].ua = false
					end
				end
				if specID == 266 then	-- Demonology Warlock
					if spellID == 146739 then	-- Corruption
						corruption = corruption - 1
						tempUnitList[targetGUID].cor = false
					end
					if spellID == 603 then	-- Doom
						doom = doom - 1
						tempUnitList[targetGUID].doom = false
					end
				end
				if specID == 258 then	-- Shadow Priest
					if spellID == 589 then		-- Shadow Word: Pain
						shadowWordPain = shadowWordPain - 1
						tempUnitList[targetGUID].swp = false
					end
					if spellID == 34914 then	-- Vampiric Touch
						vampiricTouch = vampiricTouch - 1
						tempUnitList[targetGUID].vt = false
					end
				end
			end
		end
	end
end)


--[[ Custom conditions ]]
-- Crowd control check
ProbablyEngine.condition.register("cc", function(target)
	if mirakuru.CC(target) then return false end
	return true
end)

-- Pet Special Ability check
ProbablyEngine.condition.register("pet.spell", function(target, spell)
	return IsSpellKnown(spell, true)
end)

-- Spell traveling time
ProbablyEngine.condition.register("spell.in_flight", function(target, spell)
	if not spellsWithTravelTime[tonumber(spell)] then return false end
	return spellTravelling
end)

-- Soulshard Regeneration
ProbablyEngine.condition.register("shardregen", function(target)
	if ssTimer > 0 and (GetTime() - ssTimer <= 20) then return GetTime() - ssTimer end
	return 0
end)

-- Intellect & Spellpower buffs
ProbablyEngine.condition.register("int.procs", function(target)
	local specID = GetSpecializationInfo(GetSpecialization())
	
	if specID == 267 then
		if intCount ~= 0 and intTimer - GetTime() >= mirakuru.round((2.5/((GetHaste()/100)+1)),2)+0.01 then return intCount end
	elseif specID == 266 then
		if intCount ~= 0 and intTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return intCount end
	end
	return intCount
end)

-- Critical Strike buffs
ProbablyEngine.condition.register("crit.procs", function(target)
	local specID = GetSpecializationInfo(GetSpecialization())
	
	if specID == 267 then
		if critCount ~= 0 and critTimer - GetTime() >= mirakuru.round((2.5/((GetHaste()/100)+1)),2)+0.01 then return critCount end
	elseif specID == 266 then
		if critCount ~= 0 and critTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return critCount end
	end
	return critCount
end)

-- Haste buffs
ProbablyEngine.condition.register("haste.procs", function(target)
	local specID = GetSpecializationInfo(GetSpecialization())
	
	if specID == 267 then
		if hasteCount ~= 0 and hasteTimer - GetTime() >= mirakuru.round((2.5/((GetHaste()/100)+1)),2)+0.01 then return hasteCount end
	elseif specID == 266 then
		if hasteCount ~= 0 and hasteTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return hasteCount end
	end
	return hasteCount
end)

-- Mastery buffs
ProbablyEngine.condition.register("mastery.procs", function(target)
	local specID = GetSpecializationInfo(GetSpecialization())
	
	if specID == 267 then
		if masteryCount ~= 0 and masteryTimer - GetTime() >= mirakuru.round((2.5/((GetHaste()/100)+1)),2)+0.01 then return masteryCount end
	elseif specID == 266 then
		if masteryCount ~= 0 and masteryTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return masteryCount end
	end
	return masteryCount
end)

-- Multistrike buffs
ProbablyEngine.condition.register("multistrike.procs", function(target)
	local specID = GetSpecializationInfo(GetSpecialization())
	
	if specID == 267 then
		if msCount ~= 0 and msTimer - GetTime() >= mirakuru.round((2.5/((GetHaste()/100)+1)),2)+0.01 then return msCount end
	elseif specID == 266 then
		if msCount ~= 0 and msTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return msCount end
	end
	return msCount
end)

-- Versatility buffs
ProbablyEngine.condition.register("versatility.procs", function(target)
	local specID = GetSpecializationInfo(GetSpecialization())
	
	if specID == 267 then
		if versaCount ~= 0 and versaTimer - GetTime() >= mirakuru.round((2.5/((GetHaste()/100)+1)),2)+0.01 then return versaCount end
	elseif specID == 266 then
		if versaCount ~= 0 and versaTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return versaCount end
	end
	return versaCount
end)

-- Any procs
ProbablyEngine.condition.register("proc.any", function(target)
	local specID = GetSpecializationInfo(GetSpecialization())
	if specID == 266 then
		if intCount ~= 0 and intTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return true end
		if masteryCount ~= 0 and masteryTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return true end
		if hasteCount ~= 0 and hasteTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return true end
		if msCount ~= 0 and msTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return true end
		if versaCount ~= 0 and versaTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return true end
		if critCount ~= 0 and critTimer - GetTime() >= mirakuru.round((2/((GetHaste()/100)+1)),2)+0.01 then return true end
		return false
	else return versaCount+msCount+masteryCount+hasteCount+critCount+intCount ~= 0 end
end)

-- Tier 17 Set
ProbablyEngine.condition.register("tier17", function(target)
	return tier17set
end)

-- Auspicious Spirits
ProbablyEngine.condition.register("sa", function(target)
	if target ~= "player" then return false end
	return SA_TOTAL
end)

-- Custom Boss Handler
ProbablyEngine.condition.register("isBoss", function(target)
	local bossID = mirakuru.BossIDs
	if UnitExists(target) then
		local mobID = select(6,strsplit("-", UnitGUID(target)))
		if type(mobID) ~= "number" then mobID = tonumber(mobID) end
		return bossID[mobID] ~= nil
	end
	return false
end)


-- [ ProbablyEngine ]
-- This section overrides some of the settings and functions that come bundled with
-- ProbablyEngine to add or improve functionality beyond the scope of what the AddOn
-- was originally intended to perform. By Mirakuru
-- PE Function: Redefining movement

-- Movement: Add support for Aspect of the Fox and Kil'jaeden's Cunning
ProbablyEngine.condition.register("moving", function(target)
    local speed, _ = GetUnitSpeed(target)
	if UnitBuff(target, GetSpellInfo(172106)) or UnitBuff(target, GetSpellInfo(137587)) then speed = 0 end
    return speed ~= 0
end)