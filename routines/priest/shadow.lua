--[[
	Mirakuru Profiles - Shadow Priest Combat Routine
	Created by Mirakuru
	
	Released under the GNU GENERAL PUBLIC LICENSE V2
	
	Description:
		Progression oriented Shadow PvE Profiles
]]
--[[ General Configuration ]]
local fetch = ProbablyEngine.interface.fetchKey

-- Dynamically evaluate DSL conditions
local function dynamicEval(condition, spell)
	if not condition then return false end
	return ProbablyEngine.dsl.parse(condition, spell or '')
end

-- [[ Raid Buffing function ]]

--[[ Run on first load ]]
local onLoad = function()
	-- Custom Routine buttons
	ProbablyEngine.toggle.create('GUI', 'Interface\\Icons\\trade_engineering.png"', 'GUI', 'Toggle GUI', (function() mirakuru.displayFrame(mira_shadow) end))
	
	-- Quickly reload the configuration set on load
	mirakuru.displayFrame(mira_shadow)
	mirakuru.displayFrame(mira_shadow)
end

--[[ Before Combat ]]
local beforeCombatRoutine = {
	-- Override some functions that come bundled with ProbablyEngine to add or improve functionality.
	{"", (function()
		-- We only want to override this once.
		if not firstRun then
			if FireHack then
				LineOfSight = nil
				function LineOfSight(a, b)
					-- Ignore Line of Sight on these units with very weird combat.
					local ignoreLOS = {[76585] = true,[77063] = true,[86252] = true,[83745] = true,[77182] = true,[81318] = true,[78981] = true,[86644] = true,[77252] = true,[79504] = true,[77891] = true,[77893] = true}
					local losFlags =  bit.bor(0x10, 0x100)
					local ax, ay, az = ObjectPosition(a)
					local bx, by, bz = ObjectPosition(b)
					
					-- Variables
					local aCheck = select(6,strsplit("-",UnitGUID(a)))
					local bCheck = select(6,strsplit("-",UnitGUID(b)))
					
					if ignoreLOS[tonumber(aCheck)] ~= nil then return true end
					if ignoreLOS[tonumber(bCheck)] ~= nil then return true end
					if TraceLine(ax, ay, az+2.25, bx, by, bz+2.25, losFlags) then return false end
					return true
				end
			end
			if oexecute then
				LineOfSight = nil
				function LineOfSight(a, b)
					-- Ignore Line of Sight on these units with very weird combat.
					local ignoreLOS = {[76585] = true,[77063] = true,[86252] = true,[83745] = true,[77182] = true,[81318] = true,[78981] = true,[86644] = true,[77252] = true,[79504] = true,[77891] = true,[77893] = true}
					local L = ProbablyEngine.locale.get
					
					if a ~= 'player' then ProbablyEngine.print(L('offspring_los_warn')) end
					
					-- Test variables
					local bCheck = select(6,strsplit("-",UnitGUID(b)))
					
					if ignoreLOS[tonumber(bCheck)] ~= nil then return true else return UnitInLos(b) end
				end
			end
			
			-- Only load once
			if not not ProbablyEngine.protected.unlocked then firstRun = true end
		end
	end)},
	
	--[[ Buffing ]]
	{"21562", "!player.buffs.stamina"},
	{"15473", {"player.stance = 0", (function() return fetch("miracleShadowConfig", "shadowform") end)}},
	
	-- Movement options
	{{
		-- Remove Shield
		{"/cancelaura "..GetSpellInfo(17), {"player.spell(17).cooldown = 0", "player.moving", "!player.debuff(6788)", "player.buff(17)"}},
		
		-- Body & Soul
		{"17", {"!player.debuff(6788)", "talent(2, 1)", "player.moving"}, "player"},
		
		-- Angelic Feathers
		{"121536", {"player.buff(121557).duration < 1", "player.spell(121536).charges >= 1", "player.moving"}, "player.ground"}
	}, (function() return fetch("miracleShadowConfig", "feathers") and not fetch("miracleShadowConfig", "combatfeathers") end)},
	
	-- Mass Dispel (mouse-over)
	{"!32375", {"player.spell(32375).cooldown = 0", "modifier.lalt",
		(function() return fetch("miracleShadowConfig", "massDispel") end)
	}, "mouseover.ground"},
	
	-- Force Attacking
	{"589", {"target.alive", (function() return fetch("miracleShadowConfig", "autoAttack") end)}}
}


-- [[ Manual AOE ]]
local manualAOE = {
	-- Cascade
	{"!127632", {"talent(6, 1)", "player.spell(127632).cooldown = 0"}},
	
	-- Divine Star
	{"!122121", {"talent(6, 2)", "player.spell(122121).cooldown = 0"}},
	
	-- Halo
	{"!120644", {"talent(6, 3)", "player.spell(120644).cooldown = 0"}},
	
	-- Shadow Word: Death
	{"!32379", {"player.spell(32379).cooldown = 0", "player.shadoworbs < 5"}},
	
	-- Devouring Plague
	{"2944", "player.shadoworbs = 5"},
	{"2944", {"player.moving", "player.shadoworbs >= 3"}},
	
	-- Mind Blast
	{"!8092", "player.shadoworbs < 5"},
	
	-- Searing Insanity
	{{
		{"!2944", {"!player.buff(132573)", "player.shadoworbs >= 3"}},
		{{
			{"!48045", "target.debuff(179338).duration <= 1"},
			{"48045"}
		}, "player.buff(132573)"}
	}, "talent(3, 3)"},
	
	-- No interrupterino Searing Insanity
	{"pause", (function()
		if UnitChannelInfo("player") == GetSpellInfo(179338) then return true end
		return false
	end)},
	
	-- Mind Sear
	{"!48045", "target.debuff(48045).duration <= 1"},
	{"48045"}
}


--[[ Combat Routine ]]
local combatRoutine = {
	--[[ Buffing ]]
	{"21562", "!player.buffs.stamina"},
	{"15473", {"player.stance = 0", (function() return fetch("miracleShadowConfig", "shadowform") end)}},
	
	
	--[[ Miscellaneous Settings ]]
	-- Enemy targeting
	{{
		{"/targetenemy [noexists]", "!target.exists"},
		{"/targetenemy [dead]", {"target.exists", "target.dead"}}
	}, (function() return fetch('miracleShadowConfig', 'autoTarget') end)},
	
	
	--[[ Defensive Settings ]]
	-- Mass Dispel (mouseover)
	{"!32375", {"player.spell(32375).cooldown = 0", "modifier.lalt",
		(function() return fetch("miracleShadowConfig", "massDispel") end)
	}, "mouseover.ground"},
	
	-- Healing consumables
	{{
		{"#109223"},
		{"#5512"}
	}, (function()
			if fetch("miracleShadowConfig", "useHPPot_check") then
				return dynamicEval("player.health <= "..fetch("miracleShadowConfig", "useHPPot_spin"))
			else return false end
	end)},
	
	
	--[[ Cooldown Settings ]]
	-- Arcane Torrent
	{"!28730", {"player.mana <= 90", "player.spell(28730).cooldown = 0"}},
	
	{{
		-- DPS Potion
		{{
			{"#109218", "player.hashero"},
			{"#109218", {"target.ttd > 1", "target.ttd < 50"}}
		}, (function() return fetch("miracleShadowConfig", "heroPot") end)},
		
		-- Shadowfiend / Mindbender
		{"132603", {"target.ttd > 20", "player.shadoworbs < 4", "lastcast(8092)", "!player.buff(132573)"}},
		
		-- Racials
		{{
			{"!26297", {"player.spell(26297).cooldown = 0", "!player.hashero"}},
			{"!33702", "player.spell(33702).cooldown = 0"},
		}, (function()
			if fetch("miracleShadowConfig", "racialDP") then
				return dynamicEval("player.buff(132573).duration >= 3.5")
			else return true end
		end)},
		
		-- Trinkets
		{{
			{"#trinket1"},
			{"#trinket2"},
		}, (function()
			if fetch("miracleShadowConfig", "trinketDP") then
				return dynamicEval("player.buff(132573).duration >= 3.5")
			else return true end
		end)}
	}, {"modifier.cooldowns", (function()
		if fetch("miracleShadowConfig", "bossCD") then
			return dynamicEval("target.isBoss") else return true end
	end)}},
	
	
	--[[ Movement Rotation ]]
	{{
		-- Movement abilities
		{{
			-- Remove Shield
			{"/cancelaura "..GetSpellInfo(17), {"player.spell(17).cooldown = 0", "player.moving", "!player.debuff(6788)", "player.buff(17)"}},
			
			-- Body & Soul
			{"17", {"!player.debuff(6788)", "talent(2, 1)", "player.moving"}, "player"},
			
			-- Angelic Feathers
			{"121536", {"player.buff(121557).duration < 1", "player.spell(121536).charges >= 1", "player.moving"}, "player.ground"}
		}, (function() return fetch("miracleShadowConfig", "feathers") end)},
		
		-- Power Word: Shield w/ Reflective Shield
		{"17", {"!player.debuff(6788)", "!talent(2, 1)", "player.glyph(33202)"}, "player"},
		
		-- Shadow Word: Death
		{"32379", {"!player.glyph(120583)", "target.health < 20", "player.spell(32379).cooldown = 0"}},
		
		-- Devouring Plague
		{"2944", "player.shadoworbs >= 3"},
		
		-- Level 90 Ability
		{"127632", {"target.distance <= 40", "talent(6, 1)", "player.spell(127632).cooldown = 0"}},
		{"122121", {"target.distance <= 28", "talent(6, 2)", "player.spell(122121).cooldown = 0"}},
		{"120644", {"target.distance <= 30", "talent(6, 3)", "player.spell(120644).cooldown = 0"}},
		
		-- Mind Blast
		{"8092", {"talent(7, 1)", "player.spell(8092).cooldown = 0"}},
		
		-- Shadow Word: Death (Glyphed)
		{"129176", {"player.glyph(120583)", "player.spell(129176).cooldown = 0"}}
	}, "player.moving"},
	
	
	-- [[ Manual AOE Rotation ]]
	{{{manualAOE}}, (function()
		if fetch("miracleShadowConfig", "aoeMode") ~= "automatic" then
			return dynamicEval("modifier.lshift")
		else return false end
	end)},
	
	
	--[[ Multitarget Settings ]]
	{{
		-- Shadow Word: Pain
		{"589", "mouseover.debuff(589).duration < 6", "mouseover"},
		
		-- Vampiric Touch
		{"34914", "mouseover.debuff(34914).duration < 6", "mouseover"}
	}, {"toggle.multitarget", "!player.target(mouseover)", "mouseover.enemy(player)",
		(function() return fetch("miracleShadowConfig", "mouseover_dots") end)
	}},
	
	
	-- [[ DPS Rotation ]]
	{{
		-- Clarity of Power
		{{
			-- Insanity
			{{
				-- Dotweave
				{{
					-- Don't interrupt certain casts
					{"pause", (function()
						if not dynamicEval("toggle.multitarget") then return false end
						if not fetch("miracleShadowConfig", "automatic_dotting") then return false end
						if UnitCastingInfo("player") == GetSpellInfo(34914) then return true		 end
					end)},
					
					-- 5xSO Starter
					{"2944", {"player.shadoworbs = 5", "!target.debuff(589)", "!target.debuff(34914)"}},
					
					-- Shadow Word: Death
					{"32379", {"!player.glyph(120583)", "target.health < 20", "player.shadoworbs < 5"}},
					
					-- Shadow Word: Death (Snipe)
					{{
						{"!32379", "@mirakuru._objectManager(32379)"},
					}, {"player.shadoworbs < 5", (function() return FireHack or oexecute end)}},
					
					-- Mind Blast
					{"!8092", "player.shadoworbs < 5"},
					
					-- Devouring Plague
					{"2944", {"target.debuff(589)", "target.debuff(34914)", "!target.debuff(158831)", "player.shadoworbs >= 3"}},
					
					-- Tier 17 2P/4P rotation
					{{
						-- Mind Flay: Insanity
						{{
							{"!129197", {"target.debuff(129197)", (function()
								local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
								return dynamicEval("player.spell(8092).cooldown >= "..gcd)
							end)}},
							{"129197", "!target.debuff(129197)"}
						}, {"player.buff(132573).duration >= 0.15", "player.spell(129197).cooldown = 0"}},
						
						-- Shadow Word: Pain
						{"589", {"player.shadoworbs >= 3", "!target.debuff(589)", (function()
							local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
							return dynamicEval("player.spell(8092).cooldown <= "..gcd)
						end)}},
						
						-- Vampiric Touch
						{"34914", {"!lastcast(34914)", "!target.debuff(34914)", "target.debuff(589)"}},
						
						-- So much haste
						{"pause", (function() return dynamicEval("target.debuff(34914).duration > 5") end)},
						
						-- Multidotting
						{{
							-- Vampiric Touch
							{{
								{"34914", "@mirakuru._objectManager(34914)"}
							}, {"toggle.multitarget", "!lastcast(34914)", (function() return fetch("miracleShadowConfig", "automatic_dotting") end)}},
							
							-- Shadow Word: Pain
							{{
								{"589", "@mirakuru._objectManager(589)"}
							}, {"toggle.multitarget", (function() return fetch("miracleShadowConfig", "automatic_dotting") end)}},
						}, {"player.shadoworbs < 3", "!player.buff(132573)"}},
						
						-- Mind Spike
						{"73510"}
					}, "player.tier17 >= 2"},
					
					-- Tier 17 0P rotation
					{{
						-- Mind Flay: Insanity
						{{
							{"!129197", {"target.debuff(129197)", (function()
								local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
								return dynamicEval("player.spell(8092).cooldown >= "..gcd)
							end)}},
							{"129197", "!target.debuff(129197)"}
						}, {"player.buff(132573).duration >= 0.15", "player.spell(129197).cooldown = 0"}},
						
						-- Shadow Word: Pain
						{"589", {"player.shadoworbs >= 4", "!target.debuff(589)", (function()
							local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
							return dynamicEval("player.spell(8092).cooldown <= "..gcd)
						end)}},
						
						-- Vampiric Touch
						{"34914", {"!lastcast(34914)", "!target.debuff(34914)", "target.debuff(589)"}},
						
						-- So much haste
						{"pause", (function() return dynamicEval("target.debuff(34914).duration > 5") end)},
						
						-- Multidotting
						{{
							-- Vampiric Touch
							{{
								{"34914", "@mirakuru._objectManager(34914)"}
							}, {"toggle.multitarget", "!lastcast(34914)", (function() return fetch("miracleShadowConfig", "automatic_dotting") end)}},
							
							-- Shadow Word: Pain
							{{
								{"589", "@mirakuru._objectManager(589)"}
							}, {"toggle.multitarget", (function() return fetch("miracleShadowConfig", "automatic_dotting") end)}},
						}, {"player.shadoworbs < 4", "!player.buff(132573)"}},
						
						-- Mind Spike
						{"73510"}
					}, "player.tier17 < 2"}
				}, "target.health >= 20"},
				
				-- Execute
				{{
					-- Devouring Plague
					{"2944", {"!target.debuff(158831)", "player.shadoworbs = 5"}},
					
					-- Tier 17 4P
					{"!2944", {"player.shadoworbs >= 3", (function()
						local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
						return (dynamicEval("player.buff(167254).duration < "..(gcd*1.7)) and dynamicEval("player.buff(167254).duration > "..(gcd*0.7)))
					end)}},
					
					-- Shadow Word: Death
					{"!32379", {"!player.glyph(120583)", "target.health < 20", "player.shadoworbs < 5"}},
					
					-- Shadow Word: Death (Snipe)
					{{
						{"!32379", "@mirakuru._objectManager(32379)"},
					}, {"player.shadoworbs < 5", (function() return FireHack or oexecute end)}},
					
					-- Shadow Word: Death (Glyphed)
					{"!129176", {"player.glyph(120583)", "player.spell(129176).cooldown = 0", "player.shadoworbs < 5"}},
					
					-- Mind Blast
					{"!8092", "player.shadoworbs < 5"},
					
					-- Mind Flay: Insanity
					{{
						{"129197"}
					}, {"player.buff(132573).duration >= 0.15", "player.spell(129197).cooldown = 0"}},
					
					-- Mind Spike
					{"73510"}
				}, "target.health < 20"}
			}, "talent(3, 3)"},
			
			-- Clarity of Power
			{{
				-- Don't interrupt certain casts
				{"pause", (function()
					if not dynamicEval("toggle.multitarget") then return false end
					if not fetch("miracleShadowConfig", "automatic_dotting") then return false end
					if UnitCastingInfo("player") == GetSpellInfo(34914) then return true		 end
				end)},
				
				-- Devouring Plague
				{{
					{"!2944", {"player.shadoworbs = 5", "!target.debuff(158831)"}},
					{"!2944", {"player.shadoworbs = 5", (function()
						local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
						return dynamicEval("player.spell(8092).cooldown <= "..gcd)
					end)}},
					{"!2944", {"player.shadoworbs = 5", "target.health < 20", "!player.glyph(120583)", (function()
						local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
						return dynamicEval("player.spell(32379).cooldown <= "..gcd)
					end)}},
					{"!2944", {"player.shadoworbs >= 3", (function()
						local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
						return (dynamicEval("player.buff(167254).duration < "..(gcd*1.7)) and dynamicEval("player.buff(167254).duration > "..(gcd*0.7)))
					end)}},
					{"!2944", {"player.shadoworbs >= 3", "player.tier17 < 2", (function()
						local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
						return dynamicEval("player.spell(8092).cooldown <= "..gcd)
					end)}},
					{"!2944", {"player.shadoworbs >= 3", "player.tier17 < 2", "target.health < 20", "!player.glyph(120583)", (function()
						local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
						return dynamicEval("player.spell(32379).cooldown <= "..gcd)
					end)}},
					{"!2944", {"player.shadoworbs >= 3", "player.tier17 >= 2", "player.tier17 < 4", (function()
						local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
						return dynamicEval("player.spell(8092).cooldown <= "..gcd*2)
					end)}},
					{"!2944", {"player.shadoworbs >= 3", "player.tier17 >= 2", "player.tier17 < 4", "target.health < 20", "!player.glyph(120583)", (function()
						local gcd = (1.5/((GetHaste()/100)+1)); if gcd < 1 then gcd = 1 end
						return dynamicEval("player.spell(32379).cooldown <= "..gcd*2)
					end)}}
				}, "player.shadoworbs >= 3"},
				
				-- Shadow Word: Death
				{"32379", {"!player.glyph(120583)", "target.health < 20"}},
				
				-- Shadow Word: Death (Snipe)
				{{
					{"!32379", "@mirakuru._objectManager(32379)"},
				}, {"player.shadoworbs < 5", (function() return FireHack or oexecute end)}},
				
				-- Mind Blast
				{"!8092"},
				
				-- Level 90 Ability
				{{
					-- Cascade
					{"127632", "target.distance <= 40"},
					
					-- Divine Star
					{"122121", "target.distance <= 28"},
					
					-- Halo
					{"120644", "target.distance <= 30"}
				}, (function()
						if fetch("miracleShadowConfig", "auto90") then
							if dynamicEval("player.moving") then return true end
							if dynamicEval("toggle.multitarget") then
								if fetch("miracleShadowConfig", "aoeMode") ~= "automatic" then return false end
								if IsEncounterInProgress() or (UnitExists("boss1") or UnitExists("boss2") or UnitExists("boss3") or UnitExists("boss4") or (UnitExists("target") and dynamicEval("target.isBoss"))) then
									return dynamicEval("player.area(40).enemies >= 1")
								end
							end
						end
				end)},
				
				-- Shadow Word: Death (Glyphed)
				{"!129176", {"player.glyph(120583)", "player.spell(129176).cooldown = 0", "player.shadoworbs < 5"}},
				
				-- Mind Sear
				{{
					{"!48045", "target.debuff(48045).duration <= 1"},
					{"48045"}
				}, {"player.spell(8092).cooldown > 0.5", (function()
					if fetch("miracleShadowConfig", "aoeMode") ~= "automatic" then return false else
						if not ProbablyEngine.config.read('button_states', 'multitarget', false) then return false end
						if not fetch("miracleShadowConfig", "use_sear") then return false end
						return dynamicEval("target.area(10).enemies >= "..fetch("miracleShadowConfig", "msCount"))
					end
				end)}},
				
				-- Don't interrupt Mind Sear
				{"pause", (function()
					if UnitChannelInfo("player") == GetSpellInfo(48045) then return true end
					return false
				end)},
				
				-- Surge of Darkness
				{"!73510", "player.buff(87160).count = 3"},
				
				-- Vampiric Touch (Multidotting)
				{{
					{"34914", "@mirakuru._objectManager(34914)"}
				}, {"toggle.multitarget", (function() return fetch("miracleShadowConfig", "automatic_dotting") end)}},
				
				-- Shadow Word: Pain (Multidotting)
				{{
					{"!589", "@mirakuru._objectManager(589)"}
				}, {"toggle.multitarget", (function() return fetch("miracleShadowConfig", "automatic_dotting") end)}},
				
				-- Surge of Darkness
				{"!73510", "player.buff(87160).count >= 1"},
				
				-- Mind Flay
				{"15407", {"target.debuff(158831).duration > 1"}},
				
				-- Mind Spike
				{"73510"}
			}, "!talent(3, 3)"}
		}, "talent(7, 1)"},
		
		-- [[ Auspicious Spirits ]]
		{{
			-- Searing Insanity
			{{
				{"!2944", {"!player.buff(132573)", "player.shadoworbs >= 3"}},
				{{
					{"!48045", "target.debuff(179338).duration <= 1"},
					{"48045"}
				}, "player.buff(132573)"}
			}, {"toggle.multitarget", "talent(3, 3)", (function()
				if fetch("miracleShadowConfig", "aoeMode") ~= "automatic" then return false else
					if not ProbablyEngine.config.read('button_states', 'multitarget', false) then return false end
					if not fetch("miracleShadowConfig", "use_sear") then return false end
					return dynamicEval("target.area(10).enemies >= "..fetch("miracleShadowConfig", "msCount"))
				end
			end)}},
				
			-- Don't interrupt Searing Insanity
			{"pause", (function()
				if UnitChannelInfo("player") == GetSpellInfo(179338) then return true end
				return false
			end)},
			
			
			-- Shadow Word: Death
			{"32379", {"!player.glyph(120583)", "target.health < 20", "player.shadoworbs < 5", "player.spell(32379).cooldown = 0"}},
			
			-- Shadow Word: Death (Sniping)
			{{
				{"!32379", "@mirakuru._objectManager(32379)"},
			}, {"player.shadoworbs < 5", (function() return FireHack or oexecute end)}},
			
			-- Devouring Plague
			{{
				{{
					{"!2944", "talent(3, 1)"},
					{"!2944", "player.tier17 = 4"}
				}, {"player.shadoworbs = 5", "!target.debuff(158831)"}},
				{"!2944", "player.shadoworbs = 5"},
				{"!2944", {"player.shadoworbs >= 3", "player.sa >= 3"}},
				{"!2944", {"player.shadoworbs >= 4", "player.sa >= 2"}},
				{"!2944", {"player.shadoworbs >= 3", "player.buff(167254)",
					(function()
						local gcd = (1.5/((GetHaste()/100)+1))
						return dynamicEval("player.buff(167254).duration < "..gcd)
					end),
					(function()
						local gcd = (1.5/((GetHaste()/100)+1))
						return dynamicEval("player.buff(167254).duration > "..(gcd*0.7))
					end)
				}},
				{{
					{{
						{"!2944", {"player.tier17 < 2", (function()
							local gcd = (1.5/((GetHaste()/100)+1))
							return dynamicEval("player.spell(8092).cooldown < "..gcd)
						end)}},
						{"!2944", {"target.health <= 20", (function()
							local gcd = (1.5/((GetHaste()/100)+1))
							return dynamicEval("player.spell(32379).cooldown < "..gcd)
						end)}}
					}, {"talent(3, 1)", "!target.debuff(158831)"}},
					{"!2944", {"player.tier17 < 2", (function()
						local gcd = (1.5/((GetHaste()/100)+1))
						return dynamicEval("player.spell(8092).cooldown < "..gcd)
					end)}},
					{"!2944", {"target.health <= 20", (function()
						local gcd = (1.5/((GetHaste()/100)+1))
						return dynamicEval("player.spell(32379).cooldown < "..gcd)
					end)}}
				}, "player.shadoworbs >= 4"}
			}, "player.shadoworbs >= 3"},
			
			-- Mind Blast
			{"pause", (function()
				if UnitCastingInfo("player") == GetSpellInfo(8092) then return true end
				return false
			end)},
			{"!8092", "player.spell(8092).cooldown = 0"},
			
			-- Mind Sear
			{{
				{"!48045", "target.debuff(48045).duration <= 1"},
				{"48045"}
			}, (function()
				if fetch("miracleShadowConfig", "aoeMode") ~= "automatic" then return false else
					if not ProbablyEngine.config.read('button_states', 'multitarget', false) then return false end
					if not fetch("miracleShadowConfig", "use_sear") then return false end
					return dynamicEval("target.area(10).enemies >= "..fetch("miracleShadowConfig", "msCount"))
				end
			end)},
			
			-- Don't interrupt Mind Sear
			{"pause", (function()
				if UnitChannelInfo("player") == GetSpellInfo(48045) then return true end
				return false
			end)},
			
			-- Shadow Word: Pain
			{"!589", "target.debuff(589).duration < 5.4"},
			
			-- Shadow Word: Pain (Multidotting)
			{{
				{"!589", "@mirakuru._objectManager(589)"}
			}, {"toggle.multitarget", (function() return fetch("miracleShadowConfig", "automatic_dotting") end)}},
			
			-- Insanity
			{{
				-- Mind Flay: Insanity
				{"!129197", {"player.spell(129197).cooldown = 0", "player.buff(132573)", "player.buff(132573).duration < 0.5"}},
				{"129197", {"player.spell(129197).cooldown = 0", "player.buff(132573).duration > 0.2"}}
			}, "talent(3, 3)"},
			
			-- Shadow Word: Death (Glyphed)
			{"129176", {"player.glyph(120583)", "player.spell(129176).cooldown = 0"}},
			
			-- Level 90 Ability
			{{
				-- Cascade
				{"127632", "target.distance <= 40"},
				
				-- Divine Star
				{"122121", "target.distance <= 28"},
				
				-- Halo
				{"120644", "target.distance <= 30"}
			}, {"toggle.multitarget", (function()
				if fetch("miracleShadowConfig", "auto90") then
					if IsEncounterInProgress() then return true else
						if UnitExists("boss1") or UnitExists("boss2") or UnitExists("boss3") or UnitExists("boss4") then return true end
						if UnitExists("target") then return dynamicEval("target.isBoss") end
						return false
					end
				end
			end)}},
			
			-- Surge of Darkness
			{"!73510", "player.buff(87160).count = 3"},
			
			-- Wait
			{"pause", {"player.spell(32379).cooldown < 0.5", "target.health <= 20"}},
			{"pause", "player.spell(8092).cooldown < 0.5"},
			{"pause", (function()
				if UnitChannelInfo("player") == GetSpellInfo(129197) then return true end
				return false
			end)},
			
			-- Surge of Darkness
			{"!73510", "player.buff(87160)"},
			
			-- Vampiric Touch
			{"!34914", {"!lastcast(34914)", (function()
				local cast_time = (1.5/((GetHaste()/100)+1))
				return dynamicEval("target.debuff(34914).duration < "..(15*0.3+cast_time))
			end)}},
			
			-- Vampiric Touch (Multidotting)
			{{
				{"34914", "@mirakuru._objectManager(34914)"}
			}, {"toggle.multitarget", (function() return fetch("miracleShadowConfig", "automatic_dotting") end)}},
			
			-- Mind Flay
			{{
				{"!15407", {"target.debuff(15407)", "target.debuff(15407).duration < 0.3"}},
				{"15407"}
			}}
		}, "talent(7, 3)"}
	}, (function()
		if fetch("miracleShadowConfig", "aoeMode") ~= "automatic" then
			return not dynamicEval("modifier.lshift")
		else return true end
	end)}
}



ProbablyEngine.rotation.register_custom(258, "[|cff69CFF0Mirakuru's Profiles|r] |cffFFFFFFShadow Priest|r", combatRoutine, beforeCombatRoutine, onLoad)