--[[
	Mirakuru Profiles - Affliction Combat Routine
	Created by Mirakuru
	
	Released under the GNU GENERAL PUBLIC LICENSE V2
	
	Description:
		Progression oriented Affliction PvE Profiles
]]


--[[ General Configuration ]]
local fetch = ProbablyEngine.interface.fetchKey
local function dynamicEval(condition, spell)
	if not condition then return false end
	return ProbablyEngine.dsl.parse(condition, spell or '')
end


--[[ Raid and Self Buffing ]]
local function raidBuffing()
	-- Raid buffing enabled
	if fetch("miracleAffliConfig", "buffRaid") then
		local groupType = IsInRaid() and "raid" or "party"
		for i=1, GetNumGroupMembers() do
			-- Spellpower Buff
			if not UnitBuff(groupType..i, GetSpellInfo(109773))
				and not UnitBuff(groupType..i, GetSpellInfo(1459))
				and not UnitBuff(groupType..i, GetSpellInfo(61316))
				and not UnitBuff(groupType..i, GetSpellInfo(160205))
				and not UnitBuff(groupType..i, GetSpellInfo(128433))
				and not UnitBuff(groupType..i, GetSpellInfo(90364))
				and not UnitBuff(groupType..i, GetSpellInfo(126309))
				and IsSpellInRange(GetSpellInfo(109773), groupType..i) == 1
			then return true end
			-- Multistrike Buff
			if not UnitBuff(groupType..i, GetSpellInfo(109773))
				and not UnitBuff(groupType..i, GetSpellInfo(166916))
				and not UnitBuff(groupType..i, GetSpellInfo(49868))
				and not UnitBuff(groupType..i, GetSpellInfo(113742))
				and not UnitBuff(groupType..i, GetSpellInfo(172968))
				and not UnitBuff(groupType..i, GetSpellInfo(50519))
				and not UnitBuff(groupType..i, GetSpellInfo(57386))
				and not UnitBuff(groupType..i, GetSpellInfo(58604))
				and not UnitBuff(groupType..i, GetSpellInfo(34889))
				and not UnitBuff(groupType..i, GetSpellInfo(24844))
				and IsSpellInRange(GetSpellInfo(109773), groupType..i) == 1
			then return true end
		end
	end
	
	if not GetRaidBuffTrayAuraInfo(5) or not GetRaidBuffTrayAuraInfo(8) then return true else return false end
end


--[[ Pet summoning function ]]
function Affli_pet()
	local petID = tonumber(fetch("miracleAffliConfig","summonPetSelection"))
	local spellName = GetSpellName(petID)
	
	-- Don't spam cast
	if UnitExists("pet") then return false end
	if UnitCastingInfo("player") == GetSpellInfo(petID) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112866) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112867) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112868) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112869) then return false end
	
	-- Not enough mana to cast
	if dynamicEval("player.mana < 25") then return false end
	
	-- Can't cast when on cooldown
	if GetSpellCooldown(petID) ~= 0 then return false end
	
	-- Summon the pet
	if petID == 157757 or petID == 157898 then
		if ProbablyEngine.dsl.parse("talent(7, 3)") then CastSpellByName(spellName) end
		return false
	else CastSpellByID(petID) end
end


--[[ Grimoire of Service pet function ]]
function Affli_goServ()
	local goServPet = tonumber(fetch("miracleAffliConfig","servicePetSelection"))
	local spellName = GetSpellName(goServPet)
    local start,_,_ = GetSpellCooldown(spellName)
	
	-- Sanity checks
	if not ProbablyEngine.dsl.parse("talent(5, 2)") then return false end
    if not start or start ~= 0 then return false end
	
	CastSpellByName(spellName)
end


--[[ Run on first load ]]
local onLoad = function()
	-- Custom Routine buttons
	ProbablyEngine.toggle.create('mouseover', 'Interface\\Icons\\spell_shadow_curseofsargeras.png', 'Mousover Dotting', "Enables mouseover-dotting within the combat rotation.")
	--ProbablyEngine.toggle.create('aoe', 'Interface\\Icons\\spell_shadow_seedofdestruction.png', 'AOE', "Enables the AOE rotation within the combat rotation.")
	ProbablyEngine.toggle.create('GUI', 'Interface\\Icons\\trade_engineering.png"', 'GUI', 'Toggle GUI', (function() mirakuru.displayFrame(mira_affli) end))
	
	-- Quickly reload the configuration set on load
	mirakuru.displayFrame(mira_affli)
	mirakuru.displayFrame(mira_affli)
end


--[[ Before Combat ]]
local beforeCombatRoutine = {
	-- Override some functions that come bundled with ProbablyEngine to add or improve functionality.
	{"", (function()
		-- We only want to override this once.
		if not firstRun then
			if FireHack then
				LineOfSight = nil
				function LineOfSight(a, b)
					-- Ignore Line of Sight on these units with very weird combat.
					local ignoreLOS = {[76585] = true, [77063] = true, [86252] = true, [83745] = true, [77182] = true, [81318] = true, [78981] = true}
					local losFlags =  bit.bor(0x10, 0x100)
					local ax, ay, az = ObjectPosition(a)
					local bx, by, bz = ObjectPosition(b)
					
					-- Variables
					local aCheck = select(6,strsplit("-",UnitGUID(a)))
					local bCheck = select(6,strsplit("-",UnitGUID(b)))
					
					if ignoreLOS[tonumber(aCheck)] ~= nil then return true end
					if ignoreLOS[tonumber(bCheck)] ~= nil then return true end
					if TraceLine(ax, ay, az+2.25, bx, by, bz+2.25, losFlags) then return false end
					return true
				end
			end
			if oexecute then
				LineOfSight = nil
				function LineOfSight(a, b)
					-- Ignore Line of Sight on these units with very weird combat.
					local ignoreLOS = {[76585] = true, [77063] = true, [86252] = true, [83745] = true, [77182] = true, [81318] = true, [78981] = true}
					local L = ProbablyEngine.locale.get
					
					if a ~= 'player' then ProbablyEngine.print(L('offspring_los_warn')) end
					
					-- Test variables
					local bCheck = select(6,strsplit("-",UnitGUID(b)))
					
					if ignoreLOS[tonumber(bCheck)] ~= nil then return true else return UnitInLos(b) end
				end
			end
			
			-- Only load once
			if not not ProbablyEngine.protected.unlocked then firstRun = true end
		end
	end)},
	
	--[[ Self/Raid Buffing ]]
	{"109773", (function() return raidBuffing() end)},
	
	--[[ Summon pet ]]
	{"/run Affli_pet()", {
		"!pet.exists", "!pet.alive", "!player.buff(108503)", "!player.dead",
		(function() return fetch("miracleAffliConfig", "summonPet") end)
	}},
	
	--[[ Burning Rush ]]
	{"/cancelaura "..GetSpellInfo(111400), {"!player.moving", "player.buff(111400)"}},
	{"/cancelaura "..GetSpellInfo(111400),
		(function()
			if fetch("miracleAffliConfig", "burningRush") and fetch("miracleAffliConfig", "burningRushHealth_check") then
				if dynamicEval("player.health <= "..fetch("miracleAffliConfig", "burningRushHealth_spin")) then return true end
				return false
			end
			return false
		end)
	},
	{"111400", {
		"!player.buff(111400)", "player.moving", "talent(4, 2)",
		(function()
			if fetch("miracleAffliConfig", "burningRush") then
				if fetch("miracleAffliConfig", "burningRushCombat") then return false end
				if fetch("miracleAffliConfig", "burningRushHealth_check") then
					if dynamicEval("player.health > "..fetch("miracleAffliConfig", "burningRushHealth_spin")) then return true else return false end
				else return true end
			else return false end
		end)
	}},

	--[[ Grimoire of Sacrifice ]]
	{"108503", {
		"talent(5, 3)",
		"!player.buff(108503)",
		"player.spell(108503).cooldown = 0",
		"pet.exists",
		"pet.alive"
	}},
	
	--[[ Shadowfury (Mouseover) ]]
	{"!30283", {"talent(2, 3)", "modifier.ralt", "player.spell(30283).cooldown = 0"}, "mouseover.ground"},
	
	--[[ Force Attack ]]
	{{
		{"/cast "..GetSpellInfo(17962), "target.alive"}
	}, (function() return fetch('miracleAffliConfig', 'autoAttack') end)}
}


--[[ Combat Routine ]]
local combatRoutine = {
	--[[ Self/Raid Buffing ]]
	{"109773", (function() return raidBuffing() end)},
	
	
	--[[ Miscellaneous Settings ]]
	-- Enemy targeting
	{{
		{"/targetenemy [noexists]", "!target.exists"},
		{"/targetenemy [dead]", {"target.exists", "target.dead"}}
	}, (function() return fetch('miracleAffliConfig', 'autoTarget') end)},
	
	-- Shadowfury (Mouseover)
	{"!30283", {"talent(2, 3)", "modifier.ralt", "player.spell(30283).cooldown = 0"}, "mouseover.ground"},
	
	-- Grimoire of Sacrifice
	{"108503", {
		"talent(5, 3)",
		"!player.buff(108503)",
		"player.spell(108503).cooldown = 0",
		"pet.exists",
		"pet.alive"
	}},
	
	-- Kil'jaeden's Cunning
	{"!137587", {"talent(6, 2)", "player.moving", "player.spell(137587).cooldown = 0"}},
	
	
	--[[ Defensive Settings ]]
	-- Healing consumables
	{{
		{"#5512", "player.glyph(56224)"},
		{"#109223"},
		{"#5512"}
	}, (function()
			if fetch("miracleAffliConfig", "useHPPot_check") then
				return dynamicEval("player.health <= "..fetch("miracleAffliConfig", "useHPPot_spin"))
			else return false end
	end)},
	
	-- Mortal Coil
	{"6789", {
		"talent(2, 2)", "player.spell(6789).cooldown = 0", "player.health <= 80", "target.distance < 30",
		(function() return not fetch("miracleAffliConfig", "mortalCoil_check") end)
	}},
	{"6789", {
		"talent(2, 2)", "player.spell(6789).cooldown = 0", "target.distance < 30",
		(function()
			if fetch("miracleAffliConfig", "mortalCoil_check") then
				return dynamicEval("player.health <= "..fetch("miracleAffliConfig", "mortalCoil_spin"))
			else return false end
	end)}},
	
	-- Burning Rush
	{"/cancelaura "..GetSpellInfo(111400), {"!player.moving", "player.buff(111400)"}},
	{"/cancelaura "..GetSpellInfo(111400),
		(function()
			if fetch("miracleAffliConfig", "burningRush") and fetch("miracleAffliConfig", "burningRushHealth_check") then
				return dynamicEval("player.health <= "..fetch("miracleAffliConfig", "burningRushHealth_spin"))
			end
			return false
		end)
	},
	{"111400", {
		"!player.buff(111400)", "player.moving", "talent(4, 2)",
		(function()
			if fetch("miracleAffliConfig", "burningRush") then
				if fetch("miracleAffliConfig", "burningRushHealth_check") then
					return dynamicEval("player.health > "..fetch("miracleAffliConfig", "burningRushHealth_spin"))
				else return true end
			else return false end
		end)
	}},
	
	
	--[[ Pet Settings ]]
	{"/run Affli_pet()", {
		"!pet.exists", "!pet.alive", "!player.buff(108503)", "!player.dead",
		(function()
			if fetch("miracleAffliConfig", "summonPet") then
				if fetch("miracleAffliConfig", "summonPetCombat") then
					return dynamicEval("player.buff(74434)")
				else return true end
			else return false end
		end)
	}},
	
	
	-- [[ Mouseover Dots ]]
	{{
		{"!980", "mouseover.debuff(980).duration < 6", "mouseover"},
		{"!172", "mouseover.debuff(146739).duration < 6", "mouseover"},
		{"!30108", "mouseover.debuff(30108).duration < 6", "mouseover"}
	}, {"toggle.mouseover", "!player.target(mouseover)", "mouseover.enemy(player)"}},
	
	
	-- [[ Cooldown Settings ]]
	-- Arcane Torrent
	{"!28730", {"player.mana <= 90", "player.spell(28730).cooldown = 0"}},
	
	-- Dark Soul
	{{
		-- Various cooldowns
		{{
			{"#trinket1"},
			{"#trinket2"},
			{"!26297", {"player.spell(26297).cooldown = 0", "!player.hashero"}},
			{"!33702", "player.spell(33702).cooldown = 0"},
			{"!18540", {"!talent(7, 3)", "player.spell(18540).cooldown = 0"}},
			{"!112927", {"!talent(7, 3)", "talent(5, 1)", "player.spell(112927).cooldown = 0"}},
			{"/run Affli_goServ()", "talent(5, 2)"}
		}, (function()
				if fetch("miracleAffliConfig", "poolCD") then
					return dynamicEval("player.buff(113860).duration > 8")
				else return true end
		end)},
		
		-- Draenic Intellect Potion
		{{
			{"#109218", "player.hashero"},
			{"#109218", {"target.ttd > 1", "target.ttd < 60"}}
		}, (function() return fetch("miracleAffliConfig", "heroPot") end)},
		
		
		--[[ Glyph of Dark Soul not supported - yet ]]
		{{
			-- Decide what to do with regular Dark Soul
			{{
				{"!113860", "player.int.procs > 0"},
				{"!113860", "player.crit.procs > 0"},
				{"!113860", "player.mastery.procs > 0"},
				{"!113860", "player.multistrike.procs > 0"},
				{"!113860", "player.versatility.procs > 0"},
				{"!113860", {"target.ttd > 1", "target.ttd < 60"}}
			}, {"!talent(6, 1)", "player.spell(113860).cooldown = 0"}},
			
			-- Archimonde's Darkness spices things up
			{{
				{{
					{"!113860", "player.int.procs > 0"},
					{"!113860", "player.crit.procs > 0"},
					{"!113860", "player.mastery.procs > 0"},
					{"!113860", "player.multistrike.procs > 0"},
					{"!113860", "player.versatility.procs > 0"},
				}, "player.spell(113860).charges = 2"},
				
				{{
					{"!113860", {"player.spell(113860).charges = 2", "target.ttd <= 50"}},
					{"!113860", {"player.spell(113860).charges = 1", "target.ttd <= 30"}}
				}, "target.ttd > 20"}
				
			}, {"talent(6, 1)", "player.spell(113860).charges > 0"}}
		}, "!player.glyph(159665)"}
	}, {"modifier.cooldowns",
		(function()
			if fetch("miracleAffliConfig", "bossCD") then
				return dynamicEval("target.boss")
			else return true end
		end)
	}},
	
	
	--[[ Pet Functions ]]
	-- Pet Auto-Attack
	{{
		{"/petattack", (function() return not IsPetAttackActive() end)},
		{"/petattack", (function() return not UnitIsUnit("pettarget","target") end)},
	}, {"pet.exists", "pet.alive"}},
	
	-- Command Demon
	{{
		-- TODO: Improving on pet special ability usage. (healing, interrupting, knock back, dispel)
		{"119913", "player.pet(115770).spell", "target.ground"},		-- Succubus/Shivarra
		{"119909", "player.pet(6360).spell", "target.ground"},			-- Succubus/Shivarra
		{"119911", {"player.pet(115781).spell"}},						-- Felhunter/Observer
		{"119910", {"player.pet(19467).spell"}},						-- Felhunter/Observer
		{"119907", {"player.pet(17735).spell", "target.threat < 100"}},	-- Voidwalker/Voidlord
		{"119907", {"player.pet(17735).spell", "target.threat < 100"}},	-- Voidwalker/Voidlord
		{"119905", {"player.pet(115276).spell", "player.health < 80"}},	-- Imp/Fel Imp
		{"119905", {"player.pet(89808).spell", "player.health < 80"}}	-- Imp/Fel Imp
	}, {(function() return fetch('miracleAffliConfig', 'commandDemon') end), "pet.exists", "pet.alive"}},
	
	--[[ AOE Coroutine ]]
	{{
		-- No AOE
	}},
	
	
	--[[ Main Routine ]]
	{{
		-- Pause for some casts
		{"", (function()
			if UnitCastingInfo("player") == GetSpellInfo(152108) then return true end
			if UnitCastingInfo("player") == GetSpellInfo(48181) then return true end
			if UnitCastingInfo("player") == GetSpellInfo(30108) then return true end
			return false
		end)},
		
		-- Haunt Logic
		{{
			-- Pet died.
			{"!74434", {
				"!pet.alive",
				"!pet.exists",
				"!player.dead",
				"!player.buff(108503)",
				"player.spell(691).cooldown = 0"
			}},
			
			-- Soulburn
			{{
				{"!74434", {"player.soulshards >= 3", "player.shardregen > 15"}},
				{"!74434", "player.soulshards = 4"},
				{"!74434", "player.buff(157698).duration <= 3"}
			}, {"talent(7, 1)", "!player.buff(74434)", "player.soulshards >= 2"}},
			
			-- Haunt
			{{
				-- Fuck you Haunt
				
				{{	-- Soulburn: Haunt
					{"!48181", "player.buff(74434).duration <= 3"},
					{"!48181", "player.buff(157698).duration <= 3"}
				}, {"talent(7,1)", "player.buff(74434)"}},
				
				-- Regular Haunt during Dark Soul
				{"!48181", {"target.debuff(48181).duration <= 3", "player.buff(113860).duration > 5"}},
				
				-- Target close to dying
				{{
					{"!48181", {"player.soulshards = 4", "target.ttd <= 60"}},
					{"!48181", {"player.soulshards = 3", "target.ttd <= 50"}},
					{"!48181", {"player.soulshards = 2", "target.ttd <= 40"}},
					{"!48181", {"player.soulshards = 1", "target.ttd <= 30"}}
				}, {"target.ttd > 1", "target.debuff(48181).duration < 3"}},
				
				-- Haunt when capped without SB: Haunt
				{{
					{"!48181", "player.soulshards = 4"},
					{"!48181", "player.soulshards > 2"},
					{"!48181", "player.proc.any"}
				}, {"!talent(7, 1)", "target.debuff(48181).duration < 3"}}
			}, "!player.spell(48181).in_flight"}
		}},
		
		{"!152108", {"player.spell(152108).cooldown = 0", "talent(7, 2)", "!player.casting(152108)",
			(function()
				if FireHack or oexecute then
					if fetch("miracleAffliConfig", "cleaveCata_check") then
						return dynamicEval("target.area(8).units >= "..fetch("miracleDemoConfig", "cleaveCata_spin"))
					else return true end
				else return true end
			end)
		}, "target.ground"},
		
		-- Agony
		{"!980", {"!talent(7, 2)", "target.debuff(980).duration < 7.2"}},
		{"!980", {"talent(7, 2)", "player.spell(152108).cooldown > 5", "target.debuff(980).duration < 4"}},
		
		-- Unstable Affliction
		{"!30108", {
			"!player.moving",
			"!lastcast(30108)",
			"!player.spell(30108).in_flight",
			"target.debuff(30108).duration < 5"
		}},
		
		-- Corruption
		{"!172", "target.debuff(146739).duration < 5.4"},
		
		-- Life Tap
		{"1454", "player.mana < 40"},
		
		-- Multidotting
		{{
			{"!980", "@mirakuru._objectManager(980)"},
			{"!172", "@mirakuru._objectManager(172)"},
			{"!30108", "@mirakuru._objectManager(30108)"}
		}, "modifier.multitarget"},
		
		-- Drain Soul
		{"103103"},
		
		-- While moving
		{"172", "player.moving"},
		{"1454", "player.health > 40"}
	}}
}


ProbablyEngine.rotation.register_custom(265, "[|cff69CFF0Mirakuru's Profiles|r] |cff9482C9Affliction Warlock|r", combatRoutine, beforeCombatRoutine, onLoad)