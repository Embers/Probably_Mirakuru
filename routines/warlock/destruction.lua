--[[
	Mirakuru Profiles - Destruction Combat Routine
	Created by Mirakuru
	
	Released under the GNU GENERAL PUBLIC LICENSE V2
	
	Description:
		Progression oriented Destruction PvE Profiles
]]


--[[ General Configuration ]]
local fetch = ProbablyEngine.interface.fetchKey
local function dynamicEval(condition, spell)
	if not condition then return false end
	return ProbablyEngine.dsl.parse(condition, spell or '')
end


--[[ Raid and Self Buffing ]]
local function raidBuffing()
	-- Raid buffing enabled
	if fetch("miracleDestruConfig", "buffRaid") then
		local groupType = IsInRaid() and "raid" or "party"
		for i=1, GetNumGroupMembers() do
			-- Spellpower Buff
			if not UnitBuff(groupType..i, GetSpellInfo(109773))
				and not UnitBuff(groupType..i, GetSpellInfo(1459))
				and not UnitBuff(groupType..i, GetSpellInfo(61316))
				and not UnitBuff(groupType..i, GetSpellInfo(160205))
				and not UnitBuff(groupType..i, GetSpellInfo(128433))
				and not UnitBuff(groupType..i, GetSpellInfo(90364))
				and not UnitBuff(groupType..i, GetSpellInfo(126309))
				and IsSpellInRange(GetSpellInfo(109773), groupType..i) == 1
			then return true end
			-- Multistrike Buff
			if not UnitBuff(groupType..i, GetSpellInfo(109773))
				and not UnitBuff(groupType..i, GetSpellInfo(166916))
				and not UnitBuff(groupType..i, GetSpellInfo(49868))
				and not UnitBuff(groupType..i, GetSpellInfo(113742))
				and not UnitBuff(groupType..i, GetSpellInfo(172968))
				and not UnitBuff(groupType..i, GetSpellInfo(50519))
				and not UnitBuff(groupType..i, GetSpellInfo(57386))
				and not UnitBuff(groupType..i, GetSpellInfo(58604))
				and not UnitBuff(groupType..i, GetSpellInfo(34889))
				and not UnitBuff(groupType..i, GetSpellInfo(24844))
				and IsSpellInRange(GetSpellInfo(109773), groupType..i) == 1
			then return true end
		end
	end
	
	if not GetRaidBuffTrayAuraInfo(5) or not GetRaidBuffTrayAuraInfo(8) then return true else return false end
end


--[[ Pet summoning function ]]
function Destru_pet()
	local petID = tonumber(fetch("miracleDestruConfig","summonPetSelection"))
	local spellName = GetSpellName(petID)
	
	-- Don't spam cast
	if UnitExists("pet") then return false end
	if UnitCastingInfo("player") == GetSpellInfo(petID) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112866) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112867) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112868) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112869) then return false end
	
	-- Summon the pet
	if petID == 157757 or petID == 157898 then
		if ProbablyEngine.dsl.parse("talent(7, 3)") then CastSpellByName(spellName) end
		return false
	else CastSpellByID(petID) end
end


--[[ Grimoire of Service pet function ]]
function Destru_goServ()
	local goServPet = tonumber(fetch("miracleDestruConfig","servicePetSelection"))
	local spellName = GetSpellName(goServPet)
    local start,_,_ = GetSpellCooldown(spellName)
	
	-- Sanity checks
	if not ProbablyEngine.dsl.parse("talent(5, 2)") then return false end
    if not start or start ~= 0 then return false end
	
	CastSpellByName(spellName)
end


--[[ Run on first load ]]
local onLoad = function()
	-- Custom Routine buttons
	ProbablyEngine.toggle.create('mouseover', 'Interface\\Icons\\spell_fire_felimmolation.png', 'Mousover Dotting', "Enables mouseover-dotting within the combat rotation.")
	ProbablyEngine.toggle.create('aoe', 'Interface\\Icons\\ability_warlock_fireandbrimstone.png', 'AOE', "Enables the AOE rotation within the combat rotation.")
	ProbablyEngine.toggle.create('GUI', 'Interface\\Icons\\trade_engineering.png"', 'GUI', 'Toggle GUI', (function() mirakuru.displayFrame(mira_destru) end))
	
	-- Quickly reload the configuration set on load
	mirakuru.displayFrame(mira_destru)
	mirakuru.displayFrame(mira_destru)
end


--[[ Before Combat ]]
local beforeCombatRoutine = {
	-- Override some functions that come bundled with ProbablyEngine to add or improve functionality.
	{"", (function()
		-- We only want to override this once.
		if not firstRun then
			if FireHack then
				LineOfSight = nil
				function LineOfSight(a, b)
					-- Ignore Line of Sight on these units with very weird combat.
					local ignoreLOS = {[76585] = true, [77063] = true, [86252] = true, [83745] = true, [77182] = true, [81318] = true, [78981] = true}
					local losFlags =  bit.bor(0x10, 0x100)
					local ax, ay, az = ObjectPosition(a)
					local bx, by, bz = ObjectPosition(b)
					
					-- Variables
					local aCheck = select(6,strsplit("-",UnitGUID(a)))
					local bCheck = select(6,strsplit("-",UnitGUID(b)))
					
					if ignoreLOS[tonumber(aCheck)] ~= nil then return true end
					if ignoreLOS[tonumber(bCheck)] ~= nil then return true end
					if TraceLine(ax, ay, az+2.25, bx, by, bz+2.25, losFlags) then return false end
					return true
				end
			end
			if oexecute then
				LineOfSight = nil
				function LineOfSight(a, b)
					-- Ignore Line of Sight on these units with very weird combat.
					local ignoreLOS = {[76585] = true, [77063] = true, [86252] = true, [83745] = true, [77182] = true, [81318] = true, [78981] = true}
					local L = ProbablyEngine.locale.get
					
					if a ~= 'player' then ProbablyEngine.print(L('offspring_los_warn')) end
					
					-- Test variables
					local bCheck = select(6,strsplit("-",UnitGUID(b)))
					
					if ignoreLOS[tonumber(bCheck)] ~= nil then return true else return UnitInLos(b) end
				end
			end
			
			-- Only load once
			if not not ProbablyEngine.protected.unlocked then firstRun = true end
		end
	end)},
	
	--[[ Self/Raid Buffing ]]
	{"109773", (function() return raidBuffing() end)},
	
	--[[ Summon pet ]]
	{"/run Destru_pet()", {
		"!pet.exists", "!pet.alive", "!player.buff(108503)", "timeout(petOOC, 3)", "!player.dead",
		(function() return fetch("miracleDestruConfig", "summonPet") end)
	}},
	
	--[[ Burning Rush ]]
	{"/cancelaura "..GetSpellInfo(111400), {"!player.moving", "player.buff(111400)"}},
	{"/cancelaura "..GetSpellInfo(111400),
		(function()
			if fetch("miracleDestruConfig", "burningRush") and fetch("miracleDestruConfig", "burningRushHealth_check") then
				return dynamicEval("player.health <= "..fetch("miracleDestruConfig", "burningRushHealth_spin"))
			end
			return false
		end)
	},
	{"111400", {
		"!player.buff(111400)", "player.moving", "talent(4, 2)",
		(function()
			if fetch("miracleDestruConfig", "burningRush") then
				if fetch("miracleDestruConfig", "burningRushCombat") then return false end
				if fetch("miracleDestruConfig", "burningRushHealth_check") then
					return dynamicEval("player.health > "..fetch("miracleDestruConfig", "burningRushHealth_spin"))
				else return true end
			else return false end
		end)
	}},

	--[[ Grimoire of Sacrifice ]]
	{"108503", {
		"talent(5, 3)",
		"!player.buff(108503)",
		"player.spell(108503).cooldown = 0",
		"pet.exists",
		"pet.alive"
	}},
	
	--[[ Shadowfury (Mouseover) ]]
	{"!30283", {"talent(2, 3)", "modifier.ralt", "player.spell(30283).cooldown = 0"}, "mouseover.ground"},
	
	--[[ Force Attack ]]
	{{
		{"/cast "..GetSpellInfo(17962), "target.alive"}
	}, (function() return fetch('miracleDestruConfig', 'autoAttack') end)}
}


--[[ Combat Routine ]]
local combatRoutine = {
	--[[ Self/Raid Buffing ]]
	{"109773", (function() return raidBuffing() end)},
	
	
	--[[ Miscellaneous Settings ]]
	-- Enemy targeting
	{{
		{"/targetenemy [noexists]", "!target.exists"},
		{"/targetenemy [dead]", {"target.exists", "target.dead"}}
	}, (function() return fetch('miracleDestruConfig', 'autoTarget') end)},
	
	-- Shadowfury (Mouseover)
	{"!30283", {"talent(2, 3)", "modifier.ralt", "player.spell(30283).cooldown = 0"}, "mouseover.ground"},
	
	-- Grimoire of Sacrifice
	{"108503", {
		"talent(5, 3)",
		"!player.buff(108503)",
		"player.spell(108503).cooldown = 0",
		"pet.exists",
		"pet.alive"
	}},
	
	-- Kil'jaeden's Cunning
	{"!137587", {"talent(6, 2)", "player.moving", "player.spell(137587).cooldown = 0"}},
	
	
	--[[ Defensive Settings ]]
	-- Healing consumables
	{{
		{"#5512", "player.glyph(56224)"},
		{"#109223"},
		{"#5512"}
	}, (function()
			if fetch("miracleDestruConfig", "useHPPot_check") then
				return dynamicEval("player.health <= "..fetch("miracleDestruConfig", "useHPPot_spin"))
			else return false end
	end)},
	
	-- Mortal Coil
	{"6789", {
		"talent(2, 2)", "player.spell(6789).cooldown = 0", "player.health <= 80", "target.distance < 30",
		(function() return not fetch("miracleDestruConfig", "mortalCoil_check") end)
	}},
	{"6789", {
		"talent(2, 2)", "player.spell(6789).cooldown = 0", "target.distance < 30",
		(function()
			if fetch("miracleDestruConfig", "mortalCoil_check") then
				return dynamicEval("player.health <= "..fetch("miracleDestruConfig", "mortalCoil_spin"))
			else return false end
	end)}},
	
	-- Burning Rush
	{"/cancelaura "..GetSpellInfo(111400), {"!player.moving", "player.buff(111400)"}},
	{"/cancelaura "..GetSpellInfo(111400),
		(function()
			if fetch("miracleDestruConfig", "burningRush") and fetch("miracleDestruConfig", "burningRushHealth_check") then
				return dynamicEval("player.health <= "..fetch("miracleDestruConfig", "burningRushHealth_spin"))
			end
			return false
		end)
	},
	{"111400", {
		"!player.buff(111400)", "player.moving", "talent(4, 2)",
		(function()
			if fetch("miracleDestruConfig", "burningRush") then
				if fetch("miracleDestruConfig", "burningRushHealth_check") then
					return dynamicEval("player.health > "..fetch("miracleDestruConfig", "burningRushHealth_spin"))
				else return true end
			else return false end
		end)
	}},
	
	
	--[[ Pet Settings ]]
	{"!120451", {
		"!pet.exists", "!pet.alive", "!player.buff(108503)", "!player.dead", "player.embers >= 10", "player.spell(120451).cooldown = 0",
		(function() return fetch("miracleDestruConfig", "summonPet") and fetch("miracleDestruConfig", "summonPetCombat") end)
	}},
	{"/run Destru_pet()", {
		"!pet.exists", "!pet.alive", "!player.buff(108503)", "timeout(petCombat, 1)", "!player.dead",
		(function() return fetch("miracleDestruConfig", "summonPet") and not fetch("miracleDestruConfig", "summonPetCombat") end)
	}},
	
	
	-- [[ Rain of Fire HK ]]
	{"104232", {"target.distance < 35", "modifier.lalt"}, "mouseover.ground"},
	
	
	-- [[ Mouseover Immolate ]]
	{"348", {"toggle.mouseover", "mouseover.enemy(player)", "!mouseover.debuff(157736)", "!player.target(mouseover)"}, "mouseover"},
	
	
	-- [[ Cooldown Settings ]]
	-- Arcane Torrent
	{"!28730", {"player.mana <= 90", "player.spell(28730).cooldown = 0"}},
	
	-- Dark Soul
	{{
		-- Various cooldowns
		{{
			{"#trinket1"},
			{"#trinket2"},
			{"!26297", {"player.spell(26297).cooldown = 0", "!player.hashero"}},
			{"!33702", "player.spell(33702).cooldown = 0"},
			{"!18540", {"!talent(7, 3)", "player.spell(18540).cooldown = 0"}},
			{"!112927", {"!talent(7, 3)", "talent(5, 1)", "player.spell(112927).cooldown = 0"}},
			{"/run Destru_goServ()", "talent(5, 2)"}
		}, (function()
				if fetch("miracleDestruConfig", "poolCD") then
					return dynamicEval("player.buff(113858).duration > 8")
				else return true end
		end)},
		
		-- Draenic Intellect Potion
		{{
			{"#109218", "player.hashero"},
			{"#109218", {"target.ttd > 5", "target.ttd < 45"}}
		}, (function() return fetch("miracleDestruConfig", "heroPot") end)},
		
		
		--[[ Glyph of Dark Soul not supported - yet ]]
		{{
			-- Decide what to do with regular Dark Soul
			{{
				{"!113858", "player.int.procs > 0"},
				{"!113858", "player.crit.procs > 0"},
				{"!113858", "player.mastery.procs > 0"},
				{"!113858", "player.multistrike.procs > 0"},
				{"!113858", "player.versatility.procs > 0"},
				{"!113858", {"target.ttd > 5", "target.ttd < 40"}}
			}, {"!talent(6, 1)", "player.spell(113858).cooldown = 0"}},
			
			-- Archimonde's Darkness spices things up
			{{
				{{
					{"!113858", "player.int.procs > 0"},
					{"!113858", "player.crit.procs > 0"},
					{"!113858", "player.mastery.procs > 0"},
					{"!113858", "player.multistrike.procs > 0"},
					{"!113858", "player.versatility.procs > 0"},
				}, "player.spell(113858).charges = 2"},
				
				{{
					{"!113858", {"player.spell(113858).charges = 2", "target.ttd <= 50"}},
					{"!113858", {"player.spell(113858).charges = 1", "target.ttd <= 30"}}
				}, "target.ttd > 20"}
				
			}, {"talent(6, 1)", "player.spell(113858).charges > 0", "!player.buff(113858)"}}
		}, "!player.glyph(159665)"}
	}, {"modifier.cooldowns",
		(function()
			if fetch("miracleDestruConfig", "bossCD") then
				return dynamicEval("target.boss")
			else return true end
		end)
	}},
	
	
	--[[ Pet Functions ]]
	-- Pet Auto-Attack
	{{
		{"/petattack", (function() return not IsPetAttackActive() end)},
		{"/petattack", (function() return not UnitIsUnit("pettarget","target") end)},
	}, {"pet.exists", "pet.alive"}},
	
	-- Command Demon
	{{
		-- TODO: Improving on pet special ability usage. (healing, interrupting, knock back, dispel)
		{"119913", "player.pet(115770).spell", "target.ground"},		-- Succubus/Shivarra
		{"119909", "player.pet(6360).spell", "target.ground"},			-- Succubus/Shivarra
		{"119911", {"player.pet(115781).spell"}},						-- Felhunter/Observer
		{"119910", {"player.pet(19467).spell"}},						-- Felhunter/Observer
		{"119907", {"player.pet(17735).spell", "target.threat < 100"}},	-- Voidwalker/Voidlord
		{"119907", {"player.pet(17735).spell", "target.threat < 100"}},	-- Voidwalker/Voidlord
		{"119905", {"player.pet(115276).spell", "player.health < 80"}},	-- Imp/Fel Imp
		{"119905", {"player.pet(89808).spell", "player.health < 80"}}	-- Imp/Fel Imp
	}, {(function() return fetch('miracleDestruConfig', 'commandDemon') end), "pet.exists", "pet.alive"}},
	
	--[[ AOE Coroutine ]]
	{{
		-- Automatic Mode
		{{
			-- Rain of Fire
			{"104232", {"!target.debuff(104232)", "target.distance < 35"}, "target.ground"},
			
			-- Cataclysm
			{"152108", {"talent(7, 2)", "player.spell(152108).cooldown = 0"}, "target.ground"},
			
			-- Fire and Brimstone
			{"108683", {"!player.buff(108683)",
				(function()
					if fetch('miracleDestruConfig', 'aoeEmbers_check') then
						return dynamicEval("player.embers >= "..fetch('miracleDestruConfig','aoeEmbers_spin'))
					else return dynamicEval("player.embers >= 10") end
				end)
			}},
			
			{{
				-- FnB: Immolate
				{"108686", "target.debuff(157736).duration <= 4.5"},
				
				-- FnB: Conflagrate
				{"108685", "player.spell(108685).charges = 2"},
				{"108685", "lastcast(108685)"},
				
				-- FnB: Chaos Bolt (Charred Remains)
				{"116858", {"talent(7,1)",
					(function()
						if fetch('miracleDestruConfig','aoeEmbers_check') then
							return dynamicEval("player.embers >= "..fetch('miracleDestruConfig','aoeEmbers_spin'))
						else return dynamicEval("player.embers >= 22") end
					end)
				}},
				
				-- Incinerate
				{"114654"}
			}, "player.buff(108683)"},
		}, (function()
			if fetch('miracleDestruConfig', 'aoeMode') == 'automatic' then
				if fetch('miracleDestruConfig','aoeUnits_check') then
					if UnitBuff("player",GetSpellInfo(108508)) then
						return dynamicEval("target.area(50).enemies >= "..fetch('miracleDestruConfig','aoeUnits_spin'))
					else
						return dynamicEval("target.area(10).enemies >= "..fetch('miracleDestruConfig','aoeUnits_spin'))
					end
				else
					if UnitBuff("player",GetSpellInfo(108508)) then
						return dynamicEval("target.area(50).enemies >= 4")
					else
						return dynamicEval("target.area(10).enemies >= 4")
					end
				end
			end
		end)},
		
		-- Manual Mode
		{{
			-- Rain of Fire
			{"104232", {"target.distance < 35", "!target.debuff(104232)"}, "target.ground"},
			
			-- Cataclysm
			{"152108", {"talent(7, 2)", "player.spell(152108).cooldown = 0"}, "target.ground"},
			
			-- Fire and Brimstone
			{"108683", {"talent(7,1)", "!player.buff(108683)", "player.embers >= 20"}},
			{"108683", {"!talent(7,1)", "!player.buff(108683)", "player.embers > 10"}},
			
			{{
				-- FnB: Immolate
				{"108686", "target.debuff(157736).duration <= 4.5"},
				
				-- FnB: Conflagrate
				{"108685", "player.spell(108685).charges = 2"},
				{"108685", "lastcast(108685)"},
				
				-- FnB: Chaos Bolt (Charred Remains)
				{"116858", {"talent(7,1)", "player.embers >= 20"}},
				
				-- Incinerate
				{"114654"}
			}, "player.buff(108683)"}
		}, {"modifier.control",
			(function() if fetch('miracleDestruConfig', 'aoeMode') == 'manual' then return true end end)
		}}
	}, "toggle.aoe"},
	
	-- Fire and Brimstone
	{"/cancelaura "..GetSpellInfo(108683), {"player.buff(108683)",
		(function()
			if fetch('miracleDestruConfig', 'aoeMode') == 'automatic' then
				if fetch('miracleDestruConfig','aoeUnits_check') then
					return dynamicEval("target.area(10).enemies < "..fetch('miracleDestruConfig','aoeUnits_spin'))
				else
					return dynamicEval("target.area(10).enemies < 4")
				end
			else
				return dynamicEval("modifier.control")
			end
		end)
	}},
	
	--[[ Main Routine ]]
	{{
		-- Havoc
		{{
			{"!80240", "@mirakuru._objectManager(80240)"}
		}, "modifier.multitarget"},
		
		-- Shadowburn sniping
		{{
			{"17877", "@mirakuru._objectManager(17877)"}
		}, "modifier.multitarget"},
		
		-- Shadowburn
		{{
			-- Charred Remains
			{{
				{"17877", "player.embers >= 30"},
				{"17877", {"player.embers >= 20", "lastcast(17877)"}},
				{"17877", {"player.embers >= 10", "lastcast(17877)"}},
				{"17877", "target.ttd < 15"}
			}, "talent(7,1)"},
			
			-- Not specced into Charred Remains
			{"17877", {"!talent(7,1)", "target.ttd <= 15"}}
		}, {"player.embers >= 10"}},
		
		-- Burning Ember soft cap (start)
		{"116858", {"target.debuff(157736)", "player.embers >= 35", "player.time < 5"}},
		
		-- Cataclysm
		{{
			{"152108", {"talent(7, 2)", "player.spell(152108).cooldown = 0"}, "target.ground"}
		}, (function()
				if fetch('miracleDestruConfig', 'cleaveCata_check') then
					return dynamicEval("target.area(8).enemies > "..fetch('miracleDestruConfig', 'cleaveCata_spin'))
				else return true end
		end)},
		
		-- Immolate (Emergency refresh)
		{"348", {"!talent(7,2)",
			(function() return dynamicEval("target.debuff(157736).duration <= "..mirakuru.round(1.5/((GetHaste("player")/100)+1),2)) end)}},
		{"348", {"talent(7, 2)", "player.spell(152108).cooldown > 2",
			(function() return dynamicEval("target.debuff(157736).duration <= "..mirakuru.round(1.5/((GetHaste("player")/100)+1),2)) end)}},
		
		-- Conflagrate (2nd stack)
		{"17962", {"player.spell(17962).charges = 2", "target.debuff(157736)", "!player.buff(80240).count >= 3"}},
		
		-- Chaos Bolt
		{{
			-- Dark Soul is a bloody annoying
			{{
					{"116858", "player.embers >= 30"},
					{"116858", {"player.embers >= 20", "lastcast(116858)"}},
					{"116858", {"player.embers >= 10", "lastcast(116858)"}},
					{"116858", {"player.embers >= 10", "player.buff(113858).duration <= 4"}}
			}, (function() return dynamicEval("player.buff(113858).duration >= "..mirakuru.round((2.5/((GetHaste("player")/100)+1)+0.01),2)) end)},
			
			-- With Sandman's Pouch equipped, sync with Dark Soul
			{{
				{"116858", {"talent(6, 1)",  "player.spell(113858).charges < 2"}},
				{"116858", {"!talent(6, 1)", "player.spell(113858).cooldown > 8"}}
			}, {"player.crit.procs > 0", "!player.buff(113858)", (function() return IsEquippedItem(112320) end)}},
			
			-- Without Sandman's Pouch, blow on special procs
			{{
				{"116858", "player.int.procs > 0"},
				{"116858", "player.crit.procs > 0"},
				{"116858", "player.mastery.procs > 0"},
				{"116858", "player.multistrike.procs > 0"},
				{"116858", "player.versatility.procs > 0"}
			}, (function() return not IsEquippedItem(112320) end)},
			
			-- Prevent capping resources
			{{	-- Automatic mode
				{"116858", {"talent(7, 1)", "player.embers >= 25", "!player.buff(113858)"}},
				{"116858", {"player.embers >= 35", "player.tier17 < 2"}},
				{"116858", {"player.embers >= 25", "player.tier17 >= 2"}}
			}, (function() return not fetch("miracleDestruConfig", "cbEmbers_check") end)},
			
			{{	-- Override mode
				{"116858", (function() return dynamicEval("player.embers >= "..fetch("miracleDestruConfig", "cbEmbers_spin")) end)}
			}, (function() return fetch("miracleDestruConfig", "cbEmbers_check") end)}
		}, {"player.embers >= 10", "player.buff(117828).count < 3",
			(function() if dynamicEval("talent(7, 1)") then return dynamicEval("target.health > 20") else return true end end)
		}},
		
		-- Immolate (Regular)
		{"348", {"!talent(7, 2)", "target.debuff(157736).duration <= 4.5"}},
		{"348", {"talent(7, 2)", "target.debuff(157736).duration <= 4.5", "player.spell(152108).cooldown > 2"}},
		
		-- Immolate (Multidotting)
		{{
			{"348", "@mirakuru._objectManager(348)"}
		}, "modifier.multitarget"},

		
		-- Conflagrate
		{{
			{"17962", (function() return not fetch("miracleDestruConfig", "saveConflag") end)},
			{"17962", (function() return fetch("miracleDestruConfig", "saveConflag") end)}
		}, {"player.spell(17962).charges > 0", "!player.buff(80240).count >= 3"}},
		
		-- Incinerate
		{"29722"}
	}}
}


ProbablyEngine.rotation.register_custom(267, "[|cff69CFF0Mirakuru's Profiles|r] |cff9482C9Destruction Warlock|r", combatRoutine, beforeCombatRoutine, onLoad)