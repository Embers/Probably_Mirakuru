--[[
	Mirakuru Profiles - Demonology Combat Routine
	Created by Mirakuru
	
	Released under the GNU GENERAL PUBLIC LICENSE V2
	
	Description:
		Progression oriented Demonology PvE Profiles
]]


--[[ General Configuration ]]
local fetch = ProbablyEngine.interface.fetchKey

-- Dynamically evaluate DSL conditions
local function dynamicEval(condition, spell)
	if not condition then return false end
	return ProbablyEngine.dsl.parse(condition, spell or '')
end


--[[ Raid and Self Buffing ]]
function raidBuffing()
	-- Raid buffing enabled
	if fetch("miracleDemoConfig", "buffRaid") then
		local groupType = IsInRaid() and "raid" or "party"
		for i=1, GetNumGroupMembers() do
			-- Spellpower Buff
			if not UnitBuff(groupType..i, GetSpellInfo(109773))
				and not UnitBuff(groupType..i, GetSpellInfo(1459))
				and not UnitBuff(groupType..i, GetSpellInfo(61316))
				and not UnitBuff(groupType..i, GetSpellInfo(160205))
				and not UnitBuff(groupType..i, GetSpellInfo(128433))
				and not UnitBuff(groupType..i, GetSpellInfo(90364))
				and not UnitBuff(groupType..i, GetSpellInfo(126309))
				and IsSpellInRange(GetSpellInfo(109773), groupType..i) == 1
			then return true end
			-- Multistrike Buff
			if not UnitBuff(groupType..i, GetSpellInfo(109773))
				and not UnitBuff(groupType..i, GetSpellInfo(166916))
				and not UnitBuff(groupType..i, GetSpellInfo(49868))
				and not UnitBuff(groupType..i, GetSpellInfo(113742))
				and not UnitBuff(groupType..i, GetSpellInfo(172968))
				and not UnitBuff(groupType..i, GetSpellInfo(50519))
				and not UnitBuff(groupType..i, GetSpellInfo(57386))
				and not UnitBuff(groupType..i, GetSpellInfo(58604))
				and not UnitBuff(groupType..i, GetSpellInfo(34889))
				and not UnitBuff(groupType..i, GetSpellInfo(24844))
				and IsSpellInRange(GetSpellInfo(109773), groupType..i) == 1
			then return true end
		end
	end
	
	if not GetRaidBuffTrayAuraInfo(5) or not GetRaidBuffTrayAuraInfo(8) then return true else return false end
end


--[[ Pet summoning function ]]
function Demo_pet()
	local petID = tonumber(fetch("miracleDemoConfig","summonPetSelection"))
	local spellName = GetSpellName(petID)
	
	-- Don't spam cast
	if UnitExists("pet") then return false end
	if UnitCastingInfo("player") == GetSpellInfo(petID) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112866) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112867) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112868) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112869) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(30146) then return false end
	if UnitCastingInfo("player") == GetSpellInfo(112870) then return false end
	
	-- Not enough mana
	if dynamicEval("player.mana < 25") then return false end
	
	-- Summon the pet
	if petID == 157757 or petID == 157898 then
		if ProbablyEngine.dsl.parse("talent(7, 3)") then CastSpellByName(spellName) end
		return false
	else CastSpellByID(petID) end
end


--[[ Grimoire of Service pet function ]]
function Demo_goServ()
	local goServPet = tonumber(fetch("miracleDemoConfig","servicePetSelection"))
	local spellName = GetSpellName(goServPet)
    local start,_,_ = GetSpellCooldown(spellName)
	
	-- Sanity check
	if not ProbablyEngine.dsl.parse("talent(5, 2)") then return false end
    if not start or start ~= 0 then return false end
	
	CastSpellByName(spellName)
end


--[[ Cooldowns ]]
local cooldowns = {
	-- [[ Cooldown Settings ]]
	-- Arcane Torrent
	{"!28730", {"player.mana <= 90", "player.spell(28730).cooldown = 0"}},
	
	-- Cooldown usage
	{{
		-- Various Cooldowns
		{{
			{"#trinket1"},
			{"#trinket2"},
			{"!26297", {"player.spell(26297).cooldown = 0", "!player.hashero"}},
			{"!33702", "player.spell(33702).cooldown = 0"},
			{"!18540", {"!talent(7, 3)", "player.spell(18540).cooldown = 0"}},
			{"!112927", {"!talent(7, 3)", "talent(5, 1)", "player.spell(112927).cooldown = 0"}},
			{"/run Demo_goServ()", "talent(5, 2)"}
		}, (function()
				if fetch("miracleDemoConfig", "poolCD") then
					return dynamicEval("player.buff(113861).duration > 8")
				else return true end
		end)},
		
		-- Draenic Intellect Potion
		{{
			{"#109218", "player.hashero"},
			{"#109218", {"target.ttd > 5", "target.ttd < 45"}}
		}, (function() return fetch("miracleDemoConfig", "heroPot") end)},
	}, {"modifier.cooldowns",
		(function()
			if fetch("miracleDemoConfig", "bossCD") then return dynamicEval("target.boss") else return true end
		end)
	}}
}



--[[ Run on first load ]]
local onLoad = function()
	-- Custom Routine buttons
	ProbablyEngine.toggle.create('mouseover', 'Interface\\Icons\\spell_shadow_auraofdarkness.png', 'Mousover Dotting', "Enables mouseover-dotting within the combat rotation.")
	ProbablyEngine.toggle.create('aoe', 'Interface\\Icons\\spell_fire_incinerate.png', 'AOE', "Enables the AOE rotation within the combat rotation.")
	ProbablyEngine.toggle.create('GUI', 'Interface\\Icons\\trade_engineering.png"', 'GUI', 'Toggle GUI', (function() mirakuru.displayFrame(mira_demon) end))
	
	-- Quickly reload the configuration set on load
	mirakuru.displayFrame(mira_demon)
	mirakuru.displayFrame(mira_demon)
	
	-- Codex of Xerrath changes Molten Core
	moltenCore = 122355
	if IsPlayerSpell(137206) then moltenCore = 140074 end
	if IsPlayerSpell(101508) then moltenCore = 140074 end
end


--[[ Before Combat ]]
local beforeCombatRoutine = {
	-- Override some functions that come bundled with ProbablyEngine to add or improve functionality.
	{"", (function()
		-- We only want to override this once.
		if not firstRun then
			if FireHack then
				LineOfSight = nil
				function LineOfSight(a, b)
					-- Ignore Line of Sight on these units with very weird combat.
					local ignoreLOS = {[76585] = true, [77063] = true, [86252] = true, [83745] = true, [77182] = true, [81318] = true, [78981] = true}
					local losFlags =  bit.bor(0x10, 0x100)
					local ax, ay, az = ObjectPosition(a)
					local bx, by, bz = ObjectPosition(b)
					
					-- Variables
					local aCheck = select(6,strsplit("-",UnitGUID(a)))
					local bCheck = select(6,strsplit("-",UnitGUID(b)))
					
					if ignoreLOS[tonumber(aCheck)] ~= nil then return true end
					if ignoreLOS[tonumber(bCheck)] ~= nil then return true end
					if TraceLine(ax, ay, az+2.25, bx, by, bz+2.25, losFlags) then return false end
					return true
				end
			end
			if oexecute then
				LineOfSight = nil
				function LineOfSight(a, b)
					-- Ignore Line of Sight on these units with very weird combat.
					local ignoreLOS = {[76585] = true, [77063] = true, [86252] = true, [83745] = true, [77182] = true, [81318] = true, [78981] = true}
					local L = ProbablyEngine.locale.get
					
					if a ~= 'player' then ProbablyEngine.print(L('offspring_los_warn')) end
					
					-- Test variables
					local bCheck = select(6,strsplit("-",UnitGUID(b)))
					
					if ignoreLOS[tonumber(bCheck)] ~= nil then return true else return UnitInLos(b) end
				end
			end
			
			-- Only load once
			if not not ProbablyEngine.protected.unlocked then firstRun = true end
		end
	end)},
	
	--[[ Self/Raid Buffing ]]
	{"109773", (function() return raidBuffing() end)},
	
	--[[ Summon pet ]]
	{"/run Demo_pet()", {
		"!pet.exists", "!pet.alive", "!player.buff(108503)", "timeout(petOOC, 3)", "!player.dead",
		(function() return fetch("miracleDemoConfig", "summonPet") end)
	}},
	
	--[[ Burning Rush ]]
	{"/cancelaura "..GetSpellInfo(111400), {"!player.moving", "player.buff(111400)"}},
	{"/cancelaura "..GetSpellInfo(111400),
		(function()
			if fetch("miracleDemoConfig", "burningRush") and fetch("miracleDemoConfig", "burningRushHealth_check") then
				return dynamicEval("player.health <= "..fetch("miracleDemoConfig", "burningRushHealth_spin"))
			end
			return false
		end)
	},
	{"111400", {
		"!player.buff(111400)", "player.moving", "talent(4, 2)",
		(function()
			if fetch("miracleDemoConfig", "burningRush") then
				if fetch("miracleDemoConfig", "burningRushCombat") then return false end
				if fetch("miracleDemoConfig", "burningRushHealth_check") then
					return dynamicEval("player.health > "..fetch("miracleDemoConfig", "burningRushHealth_spin"))
				else return true end
			else return false end
		end)
	}},
	
	--[[ Grimoire of Sacrifice ]]
	{"108503", {
		"talent(5, 3)",
		"!player.buff(108503)",
		"player.spell(108503).cooldown = 0",
		"pet.exists",
		"pet.alive"
	}},
	
	--[[ Shadowfury (Mouseover) ]]
	{"!30283", {"talent(2, 3)", "modifier.ralt", "player.spell(30283).cooldown = 0"}, "mouseover.ground"},
	
	--[[ Force Attack ]]
	{{
		{"/cast "..GetSpellInfo(172), "target.alive"}
	}, (function() return fetch('miracleDemoConfig', 'autoAttack') end)}
}



--[[ Combat Routine ]]
local combatRoutine = {
	--[[ Self/Raid Buffing ]]
	{"109773", (function() return raidBuffing() end)},
	
	
	--[[ Miscellaneous Settings ]]
	-- Enemy targeting
	{{
		{"/targetenemy [noexists]", "!target.exists"},
		{"/targetenemy [dead]", {"target.exists", "target.dead"}}
	}, (function() return fetch('miracleDemoConfig', 'autoTarget') end)},
	
	-- Shadowfury (Mouseover)
	{"!30283", {"talent(2, 3)", "modifier.ralt", "player.spell(30283).cooldown = 0"}, "mouseover.ground"},
	
	-- Grimoire of Sacrifice
	{"108503", {
		"talent(5, 3)",
		"!player.buff(108503)",
		"player.spell(108503).cooldown = 0",
		"pet.exists",
		"pet.alive"
	}},
	
	-- Kil'jaeden's Cunning
	{"!137587", {"talent(6, 2)", "player.moving", "player.spell(137587).cooldown = 0", "!player.debuff(157695)"}},
	
	
	--[[ Defensive Settings ]]
	-- Healing consumables
	{{
		{"#5512", "player.glyph(56224)"},
		{"#109223"},
		{"#5512"}
	}, (function()
			if fetch("miracleDemoConfig", "useHPPot_check") then
				return dynamicEval("player.health <= "..fetch("miracleDemoConfig", "useHPPot_spin"))
			else return false end
	end)},
	
	-- Mortal Coil
	{"6789", {
		"talent(2, 2)", "player.spell(6789).cooldown = 0", "player.health <= 80", "target.distance < 30",
		(function() return not fetch("miracleDemoConfig", "mortalCoil_check") end)
	}},
	{"6789", {
		"talent(2, 2)", "player.spell(6789).cooldown = 0", "target.distance < 30",
		(function()
			if fetch("miracleDemoConfig", "mortalCoil_check") then
				return dynamicEval("player.health <= "..fetch("miracleDemoConfig", "mortalCoil_spin"))
			else return false end
	end)}},
	
	-- Burning Rush
	{"/cancelaura "..GetSpellInfo(111400), {"!player.moving", "player.buff(111400)"}},
	{"/cancelaura "..GetSpellInfo(111400),
		(function()
			if fetch("miracleDemoConfig", "burningRush") and fetch("miracleDemoConfig", "burningRushHealth_check") then
				return dynamicEval("player.health <= "..fetch("miracleDemoConfig", "burningRushHealth_spin"))
			end
		end)
	},
	{"111400", {
		"!player.buff(111400)", "player.moving", "talent(4, 2)",
		(function()
			if fetch("miracleDemoConfig", "burningRush") then
				if fetch("miracleDemoConfig", "burningRushHealth_check") then
					return dynamicEval("player.health > "..fetch("miracleDemoConfig", "burningRushHealth_spin"))
				else return true end
			else return false end
		end)
	}},
	
	
	--[[ Pet Settings ]]
	{"/run Demo_pet()", {
		"!pet.exists", "!pet.alive", "!player.buff(108503)", "timeout(petCombat, 1)", "!player.dead",
		(function() return fetch("miracleDemoConfig", "summonPet") end)
	}},
	
	
	-- [[ Mouseover Dotting ]]
	{"172", {
		"toggle.mouseover",
		"player.stance = 0",
		"mouseover.distance < 40",
		"mouseover.enemy(player)",
		"!mouseover.debuff(146739)",
		"!player.target(mouseover)"
	}, "mouseover"},
	{"603", {
		"toggle.mouseover",
		"player.stance = 1",
		"!mouseover.debuff(603)",
		"mouseover.distance < 40",
		"mouseover.enemy(player)",
		"!player.target(mouseover)"
	}, "mouseover"},
	
	
	--[[ Pet Functions ]]
	-- Pet Auto-Attack
	{{
		{"/petattack", (function() return not IsPetAttackActive() end)},
		{"/petattack", (function() return not UnitIsUnit("pettarget","target") end)},
	}, {"pet.exists", "pet.alive"}},
	
	-- Command Demon
	{{
		-- TODO: Improving on pet special ability usage. (healing, interrupting, knock back, dispel)
		{"119913", "player.pet(115770).spell", "target.ground"},		-- Succubus/Shivarra
		{"119909", "player.pet(6360).spell", "target.ground"},			-- Succubus/Shivarra
		{"119911", {"player.pet(115781).spell"}},						-- Felhunter/Observer
		{"119910", {"player.pet(19467).spell"}},						-- Felhunter/Observer
		{"119907", {"player.pet(17735).spell", "target.threat < 100"}},	-- Voidwalker/Voidlord
		{"119907", {"player.pet(17735).spell", "target.threat < 100"}},	-- Voidwalker/Voidlord
		{"119905", {"player.pet(115276).spell", "player.health < 80"}},	-- Imp/Fel Imp
		{"119905", {"player.pet(89808).spell", "player.health < 80"}},	-- Imp/Fel Imp
		-- Wrathstorm
		{"119915", {
			(function()
				if FireHack or oexecute then
					if Distance("pet", "target") <= 8 then return true else return false end
				else return true end
			end),
			(function()
				if FireHack or oexecute then
					if fetch('miracleDemoConfig','cleaveStorm_check') then
						return dynamicEval("target.area(10).enemies >= "..fetch('miracleDemoConfig','cleaveStorm_spin'))
					else return true end
				else return true end
			end)
		}},
		-- Felstorm
		{"119914", {
			(function()
				if FireHack or oexecute then
					if Distance("pet", "target") <= 8 then return true else return false end
				else return true end
			end),
			(function()
				if FireHack or oexecute then
					if fetch('miracleDemoConfig','cleaveStorm_check') then
						return dynamicEval("target.area(10).enemies >= "..fetch('miracleDemoConfig','cleaveStorm_spin'))
					else return true end
				else return true end
			end)
		}}
	}, {(function() return fetch('miracleDemoConfig', 'commandDemon') end), "pet.exists", "pet.alive"}},
	
	
	-- [[ Main Routine ]]
	-- [[ Movement Subroutine ]]
	{{
		-- We can't do much in caster form...
		{{
			-- Metamorphosis
			{"!103958", "player.demonicfury > 100"},
			
			-- Hand of Gul'dan
			{{
				{"105174", {"target.debuff(47960)", "target.debuff(47960).duration < 3.4"}},
				{"105174", {"player.spell(105174).charges >= 2", "!target.debuff(47960)"}}
			}, "player.spell(105174).charges > 0"},
			
			-- Corruption
			{"172", "target.debuff(146739).duration < 5.4"},
			
			-- Multi-dot
			{{
				{"172", "@mirakuru._objectManager(172)"}
			}, "modifier.multitarget"}
		}, "player.stance = 0"},
		
		-- Demon Form
		{{
			-- Touch of Chaos
			{"103964", "player.demonicfury >= 40"}
		}, "player.stance = 1"}
	}, (function()
		if fetch("miracleDemoConfig", "moveMeta") then
			return dynamicEval("player.moving") else return false end
	end)},
	
	-- [[ Demonology AOE Subroutine ]]
	{{
		-- Mannoroth's Fury
		{"108508", {"talent(6,3)", "player.spell(108508).cooldown = 0"}},
		
		-- Caster Form
		{{
			-- Metamorphosis
			{"!103958", "player.demonicfury > 500"},
			
			-- Cataclysm
			{"!152108", {"player.spell(152108).cooldown = 0", "talent(7, 2)", "!player.casting(152108)",
				(function()
					if fetch("miracleDemoConfig", "cataMeta") then return false end
					
					if FireHack or oexecute then
						if fetch("miracleDemoConfig", "cleaveCata_check") then
							return dynamicEval("target.area(8).units >= "..fetch("miracleDemoConfig", "cleaveCata_spin"))
						else return true end
					else return true end
				end)
			}, "target.ground"},
			
			-- Corruption
			{"172", "target.debuff(146739).duration < 5.4"},
			
			-- Life Tap
			{"1454", "player.mana < 40"},
			
			-- Corruption (Multi-dot)
			{{
				{"172", "@mirakuru._objectManager(172)"}
			}, "modifier.multitarget"},
			
			-- Hellfire
			{"1949", "player.buff(1949).duration < 3"}
		}, "player.stance = 0"},
		
		-- Demon Form
		{{
			-- Immolation Aura
			{"!104025", {"player.buff(104025).duration < 3", "!player.casting(152108)"}},
			
			-- Cataclysm
			{"152108", {"player.spell(152108).cooldown = 0", "talent(7, 2)",
				(function()
					if FireHack or oexecute then
						if fetch("miracleDemoConfig", "cleaveCata_check") then
							return dynamicEval("target.area(8).units >= "..fetch("miracleDemoConfig", "cleaveCata_spin"))
						else return true end
					else return true end
				end)
			}, "target.ground"},
			
			-- Chaos Wave
			{"124916", {"player.demonicfury > 200", "player.spell(124916).charges > 0", "target.ttd < 20"}},
			
			-- Doom
			{"603", {"target.debuff(603).duration < 18",
				(function()
					if fetch("miracleDemoConfig","DoomMinHP_check") then
						return dynamicEval("target.health.actual >= "..fetch("miracleDemoConfig","DoomMinHP_spin"))
					else return true end
				end)
			}},
			
			-- Doom Multi-dot
			{{
				{"603", "@mirakuru._objectManager(603)"}
			}, "modifier.multitarget"},
			
			-- Soul Fire
			{"104027", {
				"player.demonicfury >= 160",
				(function() return dynamicEval("player.buff("..tonumber(moltenCore)..").count >= 1") end)
			}},
			
			-- Touch of Chaos
			{"103964", "player.demonicfury >= 40"}
		}, "player.stance = 1"}
	}, {"!talent(7, 1)", "toggle.aoe", "target.distance <= 8",
		(function()
			if fetch("miracleDemoConfig", "aoeMode") ~= "automatic" then
				return dynamicEval("modifier.control")
			else
				if FireHack or oexecute then
					return dynamicEval("target.area(8).enemies >= 5")
				else return true end
			end
		end)
	}},
	
	-- [[ Demonbolt Pseudo AOE Subroutine ]]
	{{
		-- Mannoroth's Fury
		{"108508", {"talent(6,3)", "player.spell(108508).cooldown = 0"}},
		
		-- Caster Form
		{{
			-- Metamorphosis
			{"!103958", "player.demonicfury > 500"},
			
			-- Corruption
			{"172", "target.debuff(146739).duration < 5.4"},
			
			-- Life Tap
			{"1454", "player.mana < 40"},
			
			-- Corruption (Multi-dot)
			{{
				{"172", "@mirakuru._objectManager(172)"}
			}, "modifier.multitarget"},
			
			-- Soul Fire
			{"6353", (function() return dynamicEval("player.buff("..tonumber(moltenCore)..").count >= 1") end)},
			
			-- Hellfire
			{"1949", "player.buff(1949).duration < 3"},
		}, "player.stance = 0"},
		
		-- Demon Form
		{{
			-- Immolation Aura
			{"!104025", {"target.distance <= 8", "player.buff(104025).duration < 3"}},
			
			-- Chaos Wave
			{"124916", {"player.demonicfury > 200", "player.spell(124916).charges > 0", "target.ttd < 20"}},
			
			-- Doom
			{"603", {"target.debuff(603).duration < 18",
				(function()
					if fetch("miracleDemoConfig","DoomMinHP_check") then
						return dynamicEval("target.health.actual >= "..fetch("miracleDemoConfig","DoomMinHP_spin"))
					else return true end
				end)
			}},
			
			-- Doom Multi-dot
			{{
				{"603", "@mirakuru._objectManager(603)"}
			}, "modifier.multitarget"},
			
			-- Soul Fire
			{"104027", {
				"player.demonicfury >= 160",
				(function() return dynamicEval("player.buff("..tonumber(moltenCore)..").count >= 1") end)
			}},
			
			-- Touch of Chaos
			{"103964", "player.demonicfury >= 40"}
		}, "player.stance = 1"}
	}, {"talent(7, 1)", "toggle.aoe", "target.distance <= 8",
		(function()
			if fetch("miracleDemoConfig", "aoeMode") ~= "automatic" then
				return dynamicEval("modifier.control")
			else
				if FireHack or oexecute then
					return dynamicEval("target.area(8).enemies >= 4")
				else return true end
			end
		end)
	}},
	
	-- [[ Demonology Subroutine ]]
	{{
		-- Regular Demonology Opener
		{{
			-- Caster Form
			{{
				-- Cooldowns
				{cooldowns},
				{{
					-- Imp Swarm
					{"104316", {"player.glyph(56242)", "player.spell(104316).cooldown = 0"}},
					
					-- Dark Soul: Knowledge
					{{
						{"113861", {"!talent(6, 1)", "player.spell(113861).cooldown = 0"}},
						{"113861", {"talent(6, 1)", "player.spell(113861).charges >= 1", "!player.buff(113861)"}}
					}},
				}, {"modifier.cooldowns",
					(function()
						if fetch("miracleDemoConfig", "bossCD") then return dynamicEval("target.boss") else return true end
					end)
				}},
				
				-- Metamorphosis
				{"103958", {
					"lastcast(105174)",
					"!target.debuff(603)",
					"player.demonicfury > 250",
					"player.spell(103958).cooldown = 0",
					"player.spell(105174).charges <= 1"
				}},
				
				-- Hand of Gul'dan
				{{
					{"105174", {"target.debuff(47960)", "target.debuff(47960).duration <= 3.5"}},
					{"105174", {"player.spell(105174).charges >= 2", "!target.debuff(47960)"}}
				}, "player.spell(105174).charges > 0"},
				
				-- Corruption
				{"172", "target.debuff(146739).duration < 5.4"},
				
				-- Shadow Bolt
				{"686"}
			}},
			
			-- Demon Form
			{{
				-- Doom
				{"603", {"!target.debuff(603)", "talent(7, 2)", "player.spell(152108).cooldown > 4",
					(function()
						if fetch("miracleDemoConfig","DoomMinHP_check") then
							return dynamicEval("target.health.actual >= "..fetch("miracleDemoConfig","DoomMinHP_spin"))
						else return true end
					end)
				}},
				{"603", {"!target.debuff(603)", "!talent(7, 2)",
					(function()
						if fetch("miracleDemoConfig","DoomMinHP_check") then
							return dynamicEval("target.health.actual >= "..fetch("miracleDemoConfig","DoomMinHP_spin"))
						else return true end
					end)
				}},
				
				-- Cataclysm
				{"152108", {"player.spell(152108).cooldown = 0", "talent(7, 2)",
					(function()
						if FireHack or oexecute then
							if fetch("miracleDemoConfig", "cleaveCata_check") then
								return dynamicEval("target.area(8).enemies >= "..fetch("miracleDemoConfig", "cleaveCata_spin"))
							else return true end
						else return true end
				end)}, "target.ground"},
				
				-- Soul Fire
				{"104027", {"player.demonicfury >= 160", (function() return dynamicEval("player.buff("..tonumber(moltenCore)..").count >= 1") end)}},
				
				-- Chaos Wave
				{"124916", {"player.tier17 = 4", "player.demonicfury >= 80", "player.spell(124916).charges > 0"}},
				
				-- Cancel Metamorphosis
				{"/cast "..GetSpellInfo(103958), {
					"target.debuff(603) > 18",
					"player.demonicfury < 80",
					"!player.casting(152108)",
					(function() if not mirakuru.opener then mirakuru.opener = true return true end end)
				}},
				
				-- Touch of Chaos
				{"103964", "player.demonicfury >= 40"}
			}, "player.stance = 1"}
		}, (function() return not mirakuru.opener end)},
		
		-- Regular Demonology
		{{
			-- Cancel Hellfire
			{"/cancelaura "..GetSpellInfo(1949), "player.casting(1949)"},
			
			-- Caster Form
			{{
				-- Cooldowns
				{cooldowns},
				
				-- Imp Swarm
				{"104316", {
					"modifier.cooldowns",
					"player.glyph(56242)",
					"player.spell(104316).cooldown = 0",
					(function()
						if fetch("miracleDemoConfig", "bossCD") then return dynamicEval("target.boss") else return true end
					end)
				}},
				
				-- Metamorphosis
				{{					
					{"103958", {"player.proc.any >= 1", "player.demonicfury >= 450"}},
					{"103958", (function() return dynamicEval("player.demonicfury >= "..fetch("miracleDemoConfig", "maxFury")) end)}
				}, "player.spell(103958).cooldown = 0"},
				
				-- Cataclysm
				{"152108", {"player.spell(152108).cooldown = 0", "talent(7, 2)",
					(function()
						if fetch("miracleDemoConfig", "cataMeta") then return false end
						
						if FireHack or oexecute then
							if fetch("miracleDemoConfig", "cleaveCata_check") then
								return dynamicEval("target.area(8).units >= "..fetch("miracleDemoConfig", "cleaveCata_spin"))
							else return true end
						else return true end
					end)
				}, "target.ground"},
				
				-- Hand of Gul'dan
				{{
					{"105174", {"player.spell(105174).charges >= 1", "player.spell(105174).recharge <= 3.5"}},
					{"105174", {"target.debuff(47960)", "target.debuff(47960).duration <= 3.5"}},
					{"105174", {"player.spell(105174).charges >= 2", "!target.debuff(47960)"}}
				}, "player.spell(105174).charges > 0"},
				
				-- Corruption
				{"172", "target.debuff(146739).duration < 5.4"},
				
				-- Corruption (Multi-dot)
				{{
					{"172", "@mirakuru._objectManager(172)"}
				}, "modifier.multitarget"},
				
				-- Soul Fire
				{"6353", {"target.health > 27", (function() return dynamicEval("player.buff("..tonumber(moltenCore)..").count > 7") end)}},
				{"6353", {"target.health < 27", (function() return dynamicEval("player.buff("..tonumber(moltenCore)..").count >= 1") end)}},
				
				-- Life Tap
				{"1454", {"player.mana < 40", "!player.buff(113861)"}},
				
				-- Shadow Bolt
				{"686"}
			}, "player.stance = 0"},
			
			-- Demon Form
			{{
				-- Dark Soul: Knowledge
				{{
					-- Glyphed
					{{
						{"!113861", {"!talent(6, 1)", "player.spell(113861).cooldown = 0"}},
						{"!113861", {"talent(6, 1)", "player.spell(113861).charges >= 1", "!player.buff(113861)"}}
					}, "player.glyph(159665)"},
					{{
						{"!113861", {"!talent(6, 1)", "player.spell(113861).cooldown = 0"}},
						{{
							{"!113861", {"talent(6, 1)", "player.spell(113861).charges >= 1", "player.proc.any >= 1"}},
							{"!113861", {"talent(6, 1)", "player.spell(113861).charges = 2", "!player.buff(113861)", "target.ttd <= 60"}},
							{"!113861", {"talent(6, 1)", "player.spell(113861).charges = 1", "!player.buff(113861)", "target.ttd <= 40"}}
						}}
					}, {"!player.glyph(159665)", "player.demonicfury > 400"}},
				}, {"modifier.cooldowns",
					(function()
						if fetch("miracleDemoConfig", "bossCD") then return dynamicEval("target.boss") else return true end
					end)
				}},
				
				-- Cataclysm
				{"152108", {"player.spell(152108).cooldown = 0", "talent(7, 2)",
					(function()
						if FireHack or oexecute then
							if fetch("miracleDemoConfig", "cleaveCata_check") then
								return dynamicEval("target.area(8).enemies >= "..fetch("miracleDemoConfig", "cleaveCata_spin"))
							else return true end
						else return true end
				end)}, "target.ground"},
					
				-- Doom
				{"603", {
					(function()
						if fetch("miracleDemoConfig","DoomMinHP_check") then
							return dynamicEval("target.health.actual >= "..fetch("miracleDemoConfig","DoomMinHP_spin"))
						else return true end
					end),
					(function()
						local start, duration, enable = GetSpellCooldown(152108)
						if start ~= 0 then timer = (start + duration - GetTime()) else timer = 0 end
						
						if dynamicEval("talent(7, 2)") then
							return dynamicEval("target.debuff(603).duration < "..(timer-1))
						else return dynamicEval("target.debuff(603).duration < 18") end
					end)
				}},
				
				-- Doom (Multi-dot)
				{{
					{"603", "@mirakuru._objectManager(603)"}
				}, "modifier.multitarget"},
				
				-- Soul Fire
				{"104027", {"player.demonicfury >= 160", (function() return dynamicEval("player.buff("..tonumber(moltenCore)..").count >= 1") end)}},
				
				-- Chaos Wave
				{"124916", {"player.tier17 = 4", "player.demonicfury >= 80", "player.spell(124916).charges > 0"}},
				
				-- Cancel Metamorphosis
				{"/cast "..GetSpellInfo(103958), {
					"!player.proc.any",
					"!player.buff(113861)",
					"!player.casting(152108)",
					(function() return dynamicEval("player.demonicfury <= "..fetch("miracleDemoConfig", "minFury")) end),
					(function()
						if fetch("miracleDemoConfig","DoomMinHP_check") then
							if dynamicEval("target.health.actual >= "..fetch("miracleDemoConfig","DoomMinHP_spin")) then
								return dynamicEval("target.debuff(603).duration > 18") else return true end
						else return dynamicEval("target.debuff(603).duration > 18") end
					end)
				}},
				
				-- Touch of Chaos
				{"103964", "player.demonicfury >= 40"}
			}, "player.stance = 1"}
		}, (function() return not not mirakuru.opener end)}
	}, {"!talent(7, 1)",
		(function()
			if fetch("miracleDemoConfig", "moveMeta") then
				return dynamicEval("!player.moving") else return true end
		end),
		(function()
			if not dynamicEval("toggle.aoe") then return true end
			
			if fetch("miracleDemoConfig", "aoeMode") ~= "automatic" then
				return not dynamicEval("modifier.control")
			else
				if FireHack or oexecute then
					return dynamicEval("target.area(8).enemies < 5")
				else return true end
			end
		end)
	}},
	
	-- [[ Demonbolt Subroutine ]]
	{{
		-- Demonbolt Opener
		{{
			-- Caster Form
			{{
				-- Cooldowns Enabled
				{{
					-- Doomguard/Terrorguard
					{{
						{"18540", "player.spell(18540).cooldown = 0"},
						{"112927", {"talent(5, 1)", "player.spell(112927).cooldown = 0"}},
					}, "!talent(7, 3)"},
					
					-- Shards of Nothing
					{"#trinket1", {
						(function()
							local itemID = GetInventoryItemID("player",13)
							if itemID == 113835 then return true else return false end
						end)
					}},
					{"#trinket2", {
						(function()
							local itemID = GetInventoryItemID("player",14)
							if itemID == 113835 then return true else return false end
						end)
					}},
					
					-- Imp Swarm
					{"104316", {"player.glyph(56242)", "player.spell(104316).cooldown = 0"}},
					
					-- Dark Soul: Knowledge
					{{
						{"113861", {
							"!talent(6, 1)",
							"target.debuff(47960)",
							"!player.buff(113861)",
							"!player.debuff(157695)",
							"player.spell(105174).charges = 1",
							"player.spell(113861).cooldown = 0",
							"target.debuff(47960).duration <= 3"
						}},
						{"113861", {
							"talent(6, 1)",
							"target.debuff(47960)",
							"!player.buff(113861)",
							"!player.debuff(157695)",
							"player.spell(105174).charges = 1",
							"player.spell(113861).charges >= 1",
							"target.debuff(47960).duration <= 3"
						}}
					}},
					
					-- Grimoire of Service
					{"/run Demo_goServ()", "talent(5, 2)"}
				}, {"modifier.cooldowns",
					(function()
						if fetch("miracleDemoConfig", "bossCD") then return dynamicEval("target.boss") else return true end
					end)
				}},
				
				-- Metamorphosis
				{"103958", {
					"target.debuff(47960)",
					"!player.debuff(157695)",
					"player.demonicfury > 330",
					"player.spell(105174).charges = 0",
					"player.spell(103958).cooldown = 0"
				}},
				
				-- Hand of Gul'dan
				{{
					{"105174", {"target.debuff(47960)", "target.debuff(47960).duration < 3"}},
					{"105174", {"player.spell(105174).charges >= 2", "!target.debuff(47960)"}}
				}, "player.spell(105174).charges > 0"},
				
				-- Corruption
				{"172", "target.debuff(146739).duration < 5.4"},
				
				-- Soul Fire
				{"6353", (function() return dynamicEval("player.buff("..tonumber(moltenCore)..").count >= 1") end)},
				
				-- Shadow Bolt
				{"686"}
			}, "player.stance = 0"},
			
			-- Demon Form
			{{
				-- Doom
				{"603", {"!target.debuff(603)",
					(function()
						if fetch("miracleDemoConfig","DoomMinHP_check") then
							return dynamicEval("target.health.actual >= "..fetch("miracleDemoConfig","DoomMinHP_spin"))
						else return true end
					end)
				}},
				
				-- Demonbolt
				{"157695", {"player.debuff(157695).count < 4",
					(function()
						local _,_,_,count = UnitDebuff("player", GetSpellInfo(157695))
						if not count then count = 1 else count = (count+1) end
						return dynamicEval("player.demonicfury > "..(80*count))
					end)
				}},
				
				-- Chaos Wave
				{"124916", {"player.tier17 = 4", "player.demonicfury > 80", "player.spell(124916).charges > 0"}},
				
				-- Cancel Metamorphosis
				{"/cast "..GetSpellInfo(103958), {
					"!player.casting",
					(function()
						if not fetch("miracleDemoConfig","DoomMinHP_check") then
							return dynamicEval("target.debuff(603)")
						else return true end
					end),
					"player.debuff(157695).count >= 2",
					(function() if not mirakuru.opener then mirakuru.opener = true return true end end)
				}}
			}, "player.stance = 1"}
		}, (function() return not mirakuru.opener end)},
		
		-- Demonbolt Regular
		{{
			-- Cancel Hellfire
			{"/cancelaura "..GetSpellInfo(1949), "player.casting(1949)"},
			
			-- Caster Form
			{{
				-- Cooldowns
				{cooldowns},
				
				-- Metamorphosis
				{{
					-- Demonbolt dump phase
					{{
						{"103958", {"!talent(6, 1)", "player.spell(113861).cooldown = 0"}},
						{"103958", {"talent(6, 1)", "player.spell(113861).charges >= 1"}}
					}, {"!player.debuff(157695)", "player.demonicfury > 900"}},
					
					-- Maxing out on Demonic Fury
					{{
						-- Dark Soul on Cooldowns
						{"103958", {
							"!talent(6, 1)", "player.spell(113861).cooldown >= 10",
							(function() return dynamicEval("player.demonicfury > "..fetch("miracleDemoConfig", "maxFury")) end)
						}},
						{"103958", {
							"talent(6, 1)", "player.spell(113861).recharge >= 10", "player.spell(113861).charges < 1",
							(function() return dynamicEval("player.demonicfury > "..fetch("miracleDemoConfig", "maxFury")) end)
						}},
						-- Demonbolt cooling down
						{"103958", {
							"player.debuff(157695).duration >= 5",
							(function() return dynamicEval("player.demonicfury > "..fetch("miracleDemoConfig", "maxFury")) end)
						}}
					}}
				}, "player.spell(103958).cooldown = 0"},
				
				-- Hand of Gul'dan
				{{
					{"105174", {"player.spell(105174).charges >= 1", "player.spell(105174).recharge <= 3.3"}},
					{"105174", {"target.debuff(47960)", "target.debuff(47960).duration <= 3.2"}},
					{"105174", {"player.spell(105174).charges >= 2", "!target.debuff(47960)"}}
				}, "player.spell(105174).charges > 0"},
				
				-- Corruption
				{"172", "target.debuff(146739).duration < 5.4"},
				
				-- Corruption (Multi-dot)
				{{
					{"172", "@mirakuru._objectManager(172)"}
				}, "modifier.multitarget"},
				
				-- Soul Fire
				{"6353", (function() return dynamicEval("player.buff("..tonumber(moltenCore)..").count >= 1") end)},
				
				-- Life Tap
				{"1454", {"player.mana < 40", "!player.buff(113861)"}},
				
				-- Shadow Bolt
				{"686"}
			}, "player.stance = 0"},
			
			-- Demon Form
			{{
				-- Leaving Metamorphosis
				{{
					-- Reached minimum fury
					{"/cast "..GetSpellInfo(103958), {
						"!player.buff(113861)", "!player.moving", "!player.casting(157695)",
						(function() return dynamicEval("player.demonicfury <= "..fetch("miracleDemoConfig", "minFury")) end),
						(function()
							if fetch("miracleDemoConfig","DoomMinHP_check") then
								if dynamicEval("target.health.actual >= "..fetch("miracleDemoConfig","DoomMinHP_spin")) then
									return dynamicEval("target.debuff(603).duration > 18") else return true end
							else return dynamicEval("target.debuff(603).duration > 18") end
						end),
						(function()
							local DBdebuff = (40/((GetHaste()/100)+1))
							if dynamicEval("player.debuff(157695)") then
								return dynamicEval("player.debuff(157695).duration < "..(DBdebuff-4))
							else return true end
						end)
					}},
					
					-- Demonbolt dump phase over
					{"/cast "..GetSpellInfo(103958), {
						"player.debuff(157695).count >= 4", "!player.casting(157695)", "!player.moving",
						(function()
							local _,_,_,_,_,_,expires = UnitDebuff("player", GetSpellInfo(157695))
							if not not expires then timer = expires - GetTime() else timer = 0 end
							
							if not fetch("miracleDemoConfig","DoomMinHP_check") then
								return dynamicEval("target.debuff(603).duration > "..timer)
							else
								if dynamicEval("target.health.actual >= "..fetch("miracleDemoConfig","DoomMinHP_spin")) then
									return dynamicEval("target.debuff(603).duration > "..timer)
								else return true end
							end
						end)
					}}
				}},
				
				-- Dark Soul: Knowledge
				{{
					{"!113861", {"!talent(6, 1)", "player.spell(113861).cooldown = 0"}},
					{"!113861", {"talent(6, 1)", "!player.buff(113861)", "player.spell(113861).charges >= 1"}}
				}, {"modifier.cooldowns", "!player.debuff(157695)", "player.demonicfury >= 890",
					(function()
						if fetch("miracleDemoConfig", "bossCD") then return dynamicEval("target.boss") else return true end
					end)
				}},
				
				-- Doom (Priority)
				{"603", {
					(function()
						if dynamicEval("!player.debuff(157695)") then
							if dynamicEval("!target.debuff(603)") then return true else
								return dynamicEval("target.debuff(603).duration < "..(4*mirakuru.round((2/((GetHaste()/100)+1)),2)+1.5)) end end
					end),
					(function()
						if fetch("miracleDemoConfig","DoomMinHP_check") then
							return dynamicEval("target.health.actual >= "..fetch("miracleDemoConfig","DoomMinHP_spin"))
						else return true end
					end),
				}},
				
				-- Demonbolt (1)
				{"157695", {"!player.debuff(157695)", "player.demonicfury >= 80",
					(function()
						local DBexecute = (2/((GetHaste()/100)+1))
						if fetch("miracleDemoConfig", "demonboltMinHP_check") then
							if UnitExists("target") then
								local LibBoss = LibStub("LibBossIDs-1.0")
								local _,_,_,_,_,mobID = strsplit("-", UnitGUID("target"))
								
								if UnitLevel("target") ~= -1 then
									if LibBoss.BossIDs[tonumber(mobID)] then return true else
										return dynamicEval("target.health.actual <= "..fetch("miracleDemoConfig", "demonboltMinHP_spin"))
									end
								else return true end
							end
						else return dynamicEval("player.buff(113861).duration >= "..DBexecute) end
					end)
				}},
				
				-- Demonbolt (cont)
				{"157695", {"player.debuff(157695)", "player.debuff(157695).count < 4",
					(function()
						local _,_,_,count = UnitDebuff("player", GetSpellInfo(157695))
						if not count then count = 1 else count = (count+1) end
						return dynamicEval("player.demonicfury >= "..(80*count))
					end)
				}},
				
				-- Doom
				{"603", {
					(function()
						local _,_,_,_,_,_,expires = UnitDebuff("player", GetSpellInfo(157695))
						if not not expires then timer = expires - GetTime() else timer = 0 end
						return dynamicEval("target.debuff(603).duration <= "..(timer+1.5))
					end),
					(function()
						if fetch("miracleDemoConfig","DoomMinHP_check") then
							return dynamicEval("target.health.actual >= "..fetch("miracleDemoConfig","DoomMinHP_spin"))
						else return true end
					end)
				}},
				
				-- Chaos Wave
				{"124916", {"player.tier17 = 4", "player.demonicfury > 80", "player.spell(124916).charges > 0"}},
				
				-- Soul Fire
				{"104027", {
					"player.demonicfury >= 160",
					(function() return dynamicEval("player.buff("..tonumber(moltenCore)..").count >= 1") end)
				}},
				
				-- Touch of Chaos
				{"103964", "player.demonicfury >= 40"}
			}, "player.stance = 1"}
		}, (function() return not not mirakuru.opener end)}
	}, {"talent(7, 1)",
		(function()
			if fetch("miracleDemoConfig", "moveMeta") then
				return dynamicEval("!player.moving") else return true end
		end),
		(function()
			if not dynamicEval("toggle.aoe") then return true end
			
			if fetch("miracleDemoConfig", "aoeMode") ~= "automatic" then
				return not dynamicEval("modifier.control")
			else
				if FireHack or oexecute then
					return dynamicEval("target.area(8).enemies < 4")
				else return true end
			end
		end)
	}}
}


ProbablyEngine.rotation.register_custom(266, "[|cff69CFF0Mirakuru's Profiles|r] |cff9482C9Demonology Warlock|r", combatRoutine, beforeCombatRoutine, onLoad)