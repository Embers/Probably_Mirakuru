--[[
	Mirakuru Profiles - Encounter Logic
	Created by Mirakuru
	
	Released under the GNU GENERAL PUBLIC LICENSE V2
	
	Description:
		None.
]]

-- Gruul
	-- Petrify
-- Oregorger
	-- Acid Torrent
	-- Retched Blackrock
	-- Energy 0%?
-- Blast Furnace
	-- Fixated
	-- Volatile Fire?
	-- Blast?
	-- Melt?
-- Hans'gar & Franzok
	-- Body slam?
	-- After shock?
	-- Disrupting Roar
	
-- Flamebender Ka'graz
	-- Blazing Radiance
	-- Firestorm
	-- Molten Torrent?
-- Kromog
	-- Rune of Grasping Hand
-- Beastlord Darmac
	-- Conflagration / Rend and Tear / Inferno Breath / Superheated Shrapnel
	-- Tantrum
	
-- Operator Thogar
	-- Delayed Siege Bomb
-- Iron Maidens
	-- Penetrating Shot
-- Blackhand