--[[
	Mirakuru Profiles - Object Manager
	Created by Mirakuru
	
	Released under the GNU GENERAL PUBLIC LICENSE V2
	
	Description:
		Contains the Object scanner and Object manager
		which Mirakuru's profiles rely on to execute its
		advanced features such as automatic AOE, multi-dotting
		and cleaving.
		
		Caches within itself every 4 frames to reduce CPU and memory
		footprint.
]]
--[[ Blacklist - Object Managers unit blacklist (won't cache matching units) ]]
local blacklist = {
		[80440] = true,		-- Ridge Condor: Horde Garrison critter
		[80437] = true,		-- Frostwall Rat: Horde Garrison critter
		[80041] = true,		-- Belly Toad: Alliance Garrison critter
		[85722] = true,		-- Rat: Alliance Garrison critter
}

--[[ Probably Engine Condition parser ]]
local function dynamicEval(condition, spell)
	if not condition then return false end
	return ProbablyEngine.dsl.parse(condition, spell or '')
end

--[[ Cache ]]
_unitCache = {}

--[[ Object Scanner ]]
local function _objectScanner(blacklist)
	wipe(_unitCache)
	
	if not LineOfSight then
		if FireHack then
			local losFlags =  bit.bor(0x10, 0x100)
			function LineOfSight(a, b)
				local ax, ay, az = ObjectPosition(a)
				local bx, by, bz = ObjectPosition(b)
				if TraceLine(ax, ay, az+2.25, bx, by, bz+2.25, losFlags) then
					return false
				end
				return true
			end
		elseif oexecute then
			function LineOfSight(a, b)
				if a ~= 'player' then
					ProbablyEngine.print(L('offspring_los_warn'))
				end
				return UnitInLos(b)
			end
		end
	end
	
	-- FireHack
	if FireHack then
		local totalObjects = ObjectCount()
		for i = 1, totalObjects do
			local object = ObjectWithIndex(i)
			local _, oType = pcall(ObjectType, object)
			if bit.band(oType, ObjectTypes.Unit) > 0 then
				if ObjectExists(object) then
					if LineOfSight("player", object) then
						if UnitCanAttack("player", object) then
							if not UnitIsPlayer(object) then
								if not UnitIsOtherPlayersPet(object) then
									if ProbablyEngine.condition["alive"](object) then
										if ProbablyEngine.condition["distance"](object) < 40 then
											if not blacklist or (blacklist and blacklist[UnitID(object)] ~= true) then
												if UnitAffectingCombat(object) then
													if (UnitIsTappedByPlayer(object) or UnitIsTappedByAllThreatList(object)) then
														_unitCache[#_unitCache+1] = {pointer = object, guid = UnitGUID(object), health = math.floor((UnitHealth(object)/UnitHealthMax(object))*100)}
													end
												end
											end
										end
									end
								end
							end
						end
					end
				end
			end
		end
	end
	
	-- OffSpring
	if oexecute then
		local totalObjects = ObjectsCount("player", 40)
		for i = 1, totalObjects do
			local pointer, id, name, bounding, x, y, z, facing, summonedbyme, createdbyme, combat, target = ObjectByIndex(i)
			local _, oType = pcall(ObjectType, pointer)
			if LineOfSight("player", pointer) then
				if UnitCanAttack("player", pointer) then
					if not UnitIsPlayer(pointer) then
						if not UnitIsOtherPlayersPet(pointer) then
							if ProbablyEngine.condition["alive"](pointer) then
								if not blacklist or (blacklist and blacklist[UnitID(pointer)] ~= true) then
									if UnitAffectingCombat(pointer) then
										if (UnitIsTappedByPlayer(pointer) or UnitIsTappedByAllThreatList(pointer)) then
											_unitCache[#_unitCache+1] = {pointer = pointer, guid = UnitGUID(pointer), health = math.floor((UnitHealth(pointer)/UnitHealthMax(pointer))*100)}
										end
									end
								end
							end
						end
					end
				end
			end
		end
	end
end

-- Throttle scans to every frame
C_Timer.NewTicker(0.1, (function()
	if ProbablyEngine.config.read('button_states', 'MasterToggle', false) then
		if ProbablyEngine.module.player.combat then _objectScanner(blacklist) end
	end
end), nil)


--[[ Object Manager ]]
function mirakuru._objectManager(spell)
	local setting = ProbablyEngine.interface.fetchKey
	local specID = GetSpecializationInfo(GetSpecialization())
	local dots = {[589] = true,[34914] = true,[348] = true,[172] = true,[980] = true,[30108] = true,[603] = true}
	
	-- Some units have buffs/debuffs that make them unattackable
	for i = 1, #_unitCache do
		if UnitBuff(_unitCache[i].pointer, GetSpellInfo(155185)) then return false end
		if UnitBuff(_unitCache[i].pointer, GetSpellInfo(155265)) then return false end
		if UnitBuff(_unitCache[i].pointer, GetSpellInfo(155266)) then return false end
		if UnitBuff(_unitCache[i].pointer, GetSpellInfo(155267)) then return false end
	end
	
	
	--[[ Special Triggers ]]
	-- Destruction Warlock
	if specID == 267 then
		local embers = UnitPower("player", SPELL_POWER_BURNING_EMBERS, true)
		
		if embers < 10 then return false end
		if not UnitExists("target") then return false end
		
		-- Havoc
		if spell == 80240 then
			local castTime = mirakuru.round((2.5/((GetHaste("player")/100)+1)),2)
			local name,_,_,_,_,endTime,_,_,_ = UnitCastingInfo("player")
			local _,_,_,cfg = UnitBuff("player",GetSpellInfo(17962))
			local DS,_,_,_,_,_,DSexpires = UnitBuff("player",GetSpellInfo(113858))
			if GetSpellCooldown(80240) ~= 0 then return false end
			
			-- Don't interrupt mid-cast Chaos Bolts
			if name == GetSpellInfo(116858) then
				if (endTime/1000 - GetTime()) >= (castTime - 0.5) then return false end
			end
			
			-- Embers setting
			if not setting("miracleDestruConfig", "cbEmbers_check") then maxEmbers = 35
				else maxEmbers = setting("miracleDestruConfig", "cbEmbers_spin") end
			
			-- Havoc execute cleaving
			for i = 1, #_unitCache do
				if mirakuru.CC(_unitCache[i].pointer) then return false end
				
				if not UnitIsUnit("target", _unitCache[i].pointer) then
					if _unitCache[i].health < 20 then
						if UnitInfront("player", _unitCache[i].pointer) then
							ProbablyEngine.dsl.parsedTarget = "target"
							return true
						end
					end
				end
			end
			
			-- Havoc cleaving
			for i = 1, #_unitCache do
				if mirakuru.CC(_unitCache[i].pointer) then return false end
				
				if _unitCache[i].health > 22 then
					if not UnitIsUnit("target", _unitCache[i].pointer) then
						if UnitInfront("player", _unitCache[i].pointer) then
							if DS then
								if embers >= (maxEmbers - 5) then
									ProbablyEngine.dsl.parsedTarget = _unitCache[i].pointer
									return true
								end
							else
								if cfg and cfg > 2 then return false else
									if embers >= maxEmbers then
										ProbablyEngine.dsl.parsedTarget = _unitCache[i].pointer
										return true
									end
								end
							end
						end
					end
				end
			end
		end
		
		-- Shadowburn
		if spell == 17877 then
			for i = 1, #_unitCache do
				-- Close to dying
				if _unitCache[i].health <= 5 then
					if UnitInfront("player", _unitCache[i].pointer) then
						ProbablyEngine.dsl.parsedTarget = _unitCache[i].pointer
						return true
					end
				end
				-- Execute range
				if _unitCache[i].health <= 20 then
					if UnitBuff("player", GetSpellInfo(80240)) then
						if UnitInfront("player", _unitCache[i].pointer) then
							ProbablyEngine.dsl.parsedTarget = _unitCache[i].pointer
							return true
						end
					end
				end
			end
		end
	end
	
	-- Shadow Priest
	if specID == 258 then
		if spell == 32379 then
			if GetSpellCooldown(32379) ~= 0 then return false end
			
			for i = 1, #_unitCache do
				if not UnitIsUnit("target", _unitCache[i].pointer) then
					if _unitCache[i].health < 20 then
						CastSpellByID(spell, _unitCache[i].pointer)
						return true
					end
				end
			end
		end
	end
	
	
	--[[ Multidotting ]]
	if dots[spell] then
		-- Destruction Warlock
		if specID == 267 then
			if immolate >= setting("miracleDestruConfig", "immoCount") then return false end
			for i = 1, #_unitCache do
				if not UnitIsUnit("target", _unitCache[i].pointer) then
					if UnitInfront("player", _unitCache[i].pointer) then
						if not UnitDebuff(_unitCache[i].pointer, GetSpellInfo(spell), nil, "PLAYER") then
							ProbablyEngine.dsl.parsedTarget = _unitCache[i].pointer
							return true
						end
					end
				end
			end
		end
		
		-- Demonology Warlock
		if specID == 266 then
			if spell == 603 then
				if doom >= setting("miracleDemoConfig", "doomCount") then return false end
			elseif spell == 172 then
				if corruption >= setting("miracleDemoConfig", "corCount") then return false end
			end
			for i = 1, #_unitCache do
				if not UnitIsUnit("target", _unitCache[i].pointer) then
					if UnitInfront("player", _unitCache[i].pointer) then
						if spell == 603 then
							if setting("miracleDemoConfig", "DoomMinHP_check") then
								if math.floor(UnitHealth(_unitCache[i].pointer)) < setting("miracleDemoConfig", "DoomMinHP_spin") then return false end
							end
						end
						if not UnitDebuff(_unitCache[i].pointer, GetSpellInfo(spell), nil, "PLAYER") then
							ProbablyEngine.dsl.parsedTarget = _unitCache[i].pointer
							return true
						end
					end
				end
			end
		end
		
		-- Affliction Warlock
		if specID == 265 then
		
			if spell == 172 then
				if corruption >= setting("miracleAffliConfig", "corCount") then return false end
			elseif spell == 30108 then
				if unstableAffliction >= setting("miracleAffliConfig", "uaCount") then return false end
			elseif spell == 980 then
				if agony >= setting("miracleAffliConfig", "agonyCount") then return false end
			end
			
			-- Spell is en-route to target
			if spell == 30108 then
				if dynamicEval("player.spell(30108).in_flight") then return false end
			end
			
			-- Don't interrupt any important spells
			if UnitCastingInfo("player") then return false end
			
			
			for i = 1, #_unitCache do
				if not UnitIsUnit("target", _unitCache[i].pointer) then
					if UnitInfront("player", _unitCache[i].pointer) then
						local _,_,_,_,_,_,expires = UnitDebuff(_unitCache[i].pointer, GetSpellInfo(spell), nil, "PLAYER")
						
						if not not expires then
							if expires - GetTime() <= 6 then
								ProbablyEngine.dsl.parsedTarget = _unitCache[i].pointer
								return true
							end
						else
							ProbablyEngine.dsl.parsedTarget = _unitCache[i].pointer
							return true
						end
					end
				end
			end
		end
		
		-- Shadow Priest
		if specID == 258 then
			local ignoreDots = {[77128] = true, [75737] = true}
		
			if spell == 589 then
				if shadowWordPain >= setting("miracleShadowConfig", "swpCount") then return false end
			elseif spell == 34914 then
				if vampiricTouch >= setting("miracleShadowConfig", "vtCount") then return false end
			end
			
			for i = 1, #_unitCache do
				local _,_,_,_,_,mobID = strsplit("-", UnitGUID(_unitCache[i].pointer))
				
				-- Don't dot mobs in this list
				if ignoreDots[mobID] then return false end
				
				if not UnitIsUnit("target", _unitCache[i].pointer) then
					if not UnitDebuff(_unitCache[i].pointer, GetSpellInfo(spell), nil, "PLAYER") then
						ProbablyEngine.dsl.parsedTarget = _unitCache[i].pointer
						return true
					end
				end
			end
		end
	end
end