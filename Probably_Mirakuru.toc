## Interface: 60100
## Title: Probably |cff69CFF0Mirakuru's Profiles|r
## Notes: Custom event handler by Mirakuru
## Dependencies: Probably
## Author: Mirakuru
## Version: 4.1.3

-- Library References
mirakuru.lua
libs\shared\objectManager.lua
libs\shared\bossIDs.lua

-- Interface Files
interface\shadow.lua
interface\affliction.lua
interface\demonology.lua
interface\destruction.lua

-- Combat Routines
routines\priest\shadow.lua
routines\warlock\affliction.lua
routines\warlock\demonology.lua
routines\warlock\destruction.lua