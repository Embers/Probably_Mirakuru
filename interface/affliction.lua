--[[
	Mirakuru Profiles - Affliction Settings GUI
	Created by Mirakuru
	
	Released under the GNU GENERAL PUBLIC LICENSE V2
	
	Description:
		Settings and Options allowing you to change
		most aspects of the Affliction Combat Routine
		to best suit your or your raids needs.
]]

mira_affli = {
	key = "miracleAffliConfig",
	profiles = true,
	title = "Affliction Warlock",
	subtitle = "Configuration",
	--color = "69CFF0",
	color = "9482C9",
	width = 250,
	height = 450,
	config = {
		--[[ Combat Configuration ]]
		{
			type = "header",
			text = "Combat Settings",
			align = "center",
		},
		{type = "rule"},
		{
			type = "checkbox",
			default = true,
			text = "|cff9482C9Raid Buffing|r",
			key = "buffRaid",
			desc = "Automatically cast Dark Intent on the raid if there is a member missing Spellpower or Multistrike."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = false,
			text = "|cff9482C9Automatic targeting|r",
			key = "autoTarget",
			desc = "Automatically selects a new target if you have no target or your target dies while in combat."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = false,
			text = "|cff9482C9Force Attacking|r",
			key = "autoAttack",
			desc = "Automatically attacks your current target even if you're not in combat. (Best suited for farming)"
		},
		{type = "spacer"},
		{
			type = "checkspin",
			default_check = true,
			default_spin = 2,
			width = 50,
			min = 1,
			max = 10,
			step = 1,
			text = "|cff9482C9Use Cataclysm for cleaving|r",
			key = "cleaveCata",
			desc = "Will delay Cataclysm where possible to allow it to cleave off as many targets as possible."
		},
		--[[{type = "spacer"},{type = "spacer"},
		{
			type = "dropdown",
			text = "|cff9482C9Select AoE Mode:|r",
			key = "aoeMode",
			list = {
				{
					text = "Automatic",
					key = "automatic"
				},
				{
					text = "Manual",
					key = "manual"
				}
			},
			desc = "Select the AoE Mode which best suits you.",
			default = "automatic",
		},]]
		
		--[[ Dotting Configuration ]]
		{type = "spacer"},{type = "spacer"},
		{
			type = "header",
			text = "DOT Settings",
			align = "center",
		},
		{type = "rule"},
		{
			type = "spinner",
			default = 4,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "|cff9482C9Agony|r",
			key = "agonyCount",
			desc = "Determines how many Agonies to maintain while in combat."
		},
		{type = "spacer"},
		{
			type = "spinner",
			default = 4,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "|cff9482C9Corruption|r",
			key = "corCount",
			desc = "Determines how many Corruptions to maintain while in combat."
		},
		{type = "spacer"},
		{
			type = "spinner",
			default = 4,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "|cff9482C9Unstable Affliction|r",
			key = "uaCount",
			desc = "Determines how many Unstable Afflictions to maintain while in combat."
		},

		
		--[[ Pet Configuration ]]
		{type = "spacer"},{type = "spacer"},
		{
			type = "header",
			text = "Pet Settings",
			align = "center",
		},
		{type = "rule"},
		{
			type = "checkbox",
			default = true,
			text = "|cff9482C9Use Command Demon|r",
			key = "commandDemon",
			desc = "While enabled the routine will automatically attempt to use Command Demon as optimally as possible."
		},
		{
			type = "checkbox",
			default = true,
			text = "|cff9482C9Summon Pet|r",
			key = "summonPet",
			desc = "While enabled the routine will automatically summon your selected pet."
		},
		{
			type = "checkbox",
			default = true,
			text = "|cff9482C9Summon Pet with instant abilities|r",
			key = "summonPetCombat",
			desc = "While enabled the routine will only summon your pet in combat using instant abilities."
		},
		{type = "spacer"},
		{
			type = "dropdown",
			text = "|cff9482C9Select Pet:|r",
			key = "summonPetSelection",
			list = {
				{
					text = "Imp",
					key = "688"
				},
				{
					text = "Voidwalker",
					key = "697"
				},
				{
					text = "Felhunter",
					key = "691"
				},
				{
					text = "Succubus",
					key = "712"
				},
				{
					text = "Doomguard",
					key = "157757"
				},
				{
					text = "Infernal",
					key = "157898"
				}
			},
			desc = "Select which pet to summon.",
			default = "691",
		},
		
		--[[ Cooldown Configuration ]]
		{type = "spacer"},{type = "spacer"},
		{
			type = "header",
			text = "Cooldown Settings",
			align = "center",
		},
		{type = "rule"},
		{
			type = "checkbox",
			default = true,
			text = "|cff9482C9Save cooldowns for bosses|r",
			key = "bossCD",
			desc = "Will save all cooldowns available for boss encounters."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "|cff9482C9Pool Cooldowns|r",
			key = "poolCD",
			desc = "With this option selected, the routine will try and use all cooldowns together as optimally as possible."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "|cff9482C9Automatically use DPS Potion|r",
			key = "heroPot",
			desc = "Automatically uses your DPS potion to its full potential."
		},
		{type = "spacer"},
		{
			type = "dropdown",
			text = "|cff9482C9Grimoire of Service:|r",
			key = "servicePetSelection",
			list = {
				{
					text = "Imp",
					key = "111859"
				},
				{
					text = "Voidwalker",
					key = "111895"
				},
				{
					text = "Felhunter",
					key = "111897"
				},
				{
					text = "Succubus",
					key = "111896"
				},
				{
					text = "Doomguard",
					key = "157906"
				},
				{
					text = "Infernal",
					key = "157907"
				}
			},
			desc = "Select which pet to summon.",
			default = "111897",
		},
		
		--[[ Defensive Configuration ]]
		{type = "spacer"},{type = "spacer"},
		{
			type = "header",
			text = "Defensive Settings",
			align = "center",
		},
		{type = "rule"},
		{
			type = "checkspin",
			default_check = true,
			default_spin = 35,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "|cff9482C9Healing Consumables|r",
			key = "useHPPot",
			desc = "While enabled the combat routine will automatically use Health Potion or Healthstone at the set health percentage depending on your glyphs."
		},
		{type = "spacer"},
		{
			type = "checkspin",
			default_check = false,
			default_spin = 80,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "|cffDC143COverride:|r |cff9482C9Mortal Coil|r",
			key = "mortalCoil",
			desc = "While enabled the routine will use Mortal Coil when you drop bellow the set health percentage."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "|cff9482C9Automatically use Burning Rush on movement|r",
			key = "burningRush",
			desc = "While enabled the routine will use Burning Rush while you're moving."
		},
		{
			type = "checkbox",
			default = true,
			text = "|cff9482C9... only use Burning Rush in combat|r",
			key = "burningRushCombat",
			desc = "While enabled the routine will only use Burning Rush while in combat."
		},
		{
			type = "checkspin",
			default_check = false,
			default_spin = 70,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "|cff9482C9Burning Rush minimum health|r",
			key = "burningRushHealth",
			desc = "While enabled the routine will not use Burning Rush bellow this health percentage."
		},
	}
}