--[[
	Mirakuru Profiles - Shadow Priest Settings GUI
	Created by Mirakuru
	
	Released under the GNU GENERAL PUBLIC LICENSE V2
	
	Description:
		Settings and Options allowing you to change
		most aspects of the Affliction Combat Routine
		to best suit your or your raids needs.
]]

mira_shadow = {
	key = "miracleShadowConfig",
	profiles = true,
	title = "Shadow Priest",
	subtitle = "Configuration",
	color = "3C4133",
	width = 250,
	height = 450,
	config = {
		--[[ Header Texture ]]
		{
			type = "texture",
			texture = "Interface\\AddOns\\Probably_Mirakuru\\interface\\media\\priest.blp",
			width = 240,
			height = 240,
			center = true,
			offset = 120,
			y = 90
		},
		
		--[[ Combat Configuration ]]
		{
			type = "header",
			text = "Combat Settings",
			align = "center",
		},
		{type = "rule"},
		{
			type = "checkbox",
			default = true,
			text = "Raid Buffing",
			key = "buffRaid",
			desc = "Automatically cast Power Word: Fortitude on the raid if there is a member missing Stamina."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "Shadowform",
			key = "shadowform",
			desc = "Automatically keep Shadowform on yourself."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = false,
			text = "Automatic targeting",
			key = "autoTarget",
			desc = "Automatically selects a new target if you have no target or your target dies while in combat."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = false,
			text = "Force Attacking",
			key = "autoAttack",
			desc = "Automatically attacks your current target even if you're not in combat. (Best suited for farming)"
		},
		{type = "spacer"},
		
		--[[ Multitarget Configuration ]]
		{type = "spacer"},{type = "spacer"},
		{
			type = "header",
			text = "Multitarget Settings",
			align = "center",
		},
		{type = "rule"},
		{
			type = "spinner",
			default = 5,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "Shadow Word: Pain",
			key = "swpCount",
			desc = "Max amount of units to keep Shadow Word: Pain on during combat."
		},
		{type = "spacer"},
		{
			type = "spinner",
			default = 5,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "Vampiric Touch",
			key = "vtCount",
			desc = "Max amount of units to keep Vampiric Touch on during combat."
		},
		{type = "spacer"},
		{
			type = "spinner",
			default = 4,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "Mind Sear Unit Count",
			key = "msCount",
			desc = "Minimum amount of units in range of your target for Mind Sear"
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "Use Level 90 Talent",
			key = "auto90",
			desc = "While toggled on, this will always make sure that you're casting your level 90 talent optimally. It will however be suppressed on trash to avoid pulling extra trash/mobs."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "Mouse-over Dotting",
			key = "mouseover_dots",
			desc = "Enables or disables allowing the routine to cast dots on targets who you are mousing over."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "Automatic Dotting",
			key = "automatic_dotting",
			desc = "Enables or disables automatic dotting within the routine."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "Use Mind Sear / Searing Insanity",
			key = "use_sear",
			desc = "Enables or disables automatic dotting within the routine."
		},
		{type = "spacer"},{type = "spacer"},
		{
			type = "dropdown",
			text = "AOE Mode:",
			key = "aoeMode",
			list = {
				{
					text = "Automatic",
					key = "automatic"
				},
				{
					text = "Manual",
					key = "manual"
				}
			},
			desc = "Select the AoE Mode which best suits you. Manual mode require you to hold down Left Shift and ignores some of the above settings.",
			default = "automatic",
		},
		
		--[[ Cooldown Configuration ]]
		{type = "spacer"},{type = "spacer"},
		{
			type = "header",
			text = "Cooldown Settings",
			align = "center",
		},
		{type = "rule"},
		{
			type = "checkbox",
			default = true,
			text = "Save cooldowns for bosses",
			key = "bossCD",
			desc = "Will save all cooldowns available for boss encounters."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "Automatically use DPS Potion",
			key = "heroPot",
			desc = "Automatically uses your DPS potion to its full potential."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "Racials on Devouring Plague",
			key = "racialDP",
			desc = "Automatically use any Racials before casting Devouring Plague"
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "On-use Trinkets before Devouring Plague",
			key = "trinketDP",
			desc = "Automatically use on-use trinkets before Devouring Plague"
		},
		
		--[[ Defensive Configuration ]]
		{type = "spacer"},{type = "spacer"},
		{
			type = "header",
			text = "Defensive Settings",
			align = "center",
		},
		{type = "rule"},
		{
			type = "checkbox",
			default = true,
			text = "Mass Dispel on mouseover location",
			key = "massDispel",
			desc = "While enabled allows the routine to cast Mass Dispel on your mouseover location when Left Alt is held down."
		},
		{type = "spacer"},
		{
			type = "checkspin",
			default_check = true,
			default_spin = 45,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "Healing Consumables",
			key = "useHPPot",
			desc = "While enabled the combat routine will automatically use Healing Tonic or Healthstone at the set health percentage."
		},
		{type = "spacer"},
		{
			type = "checkspin",
			default_check = true,
			default_spin = 20,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "Emergency Dispersion",
			key = "dispersion",
			desc = "While enabled the combat routine will automatically use Dispersion when you drop below this health percentage."
		},
		{type = "spacer"},
		{
			type = "checkbox",
			default = true,
			text = "Speed Increasers",
			key = "feathers",
			desc = "Automatically use Speed buffs. (Requires Body & Soul or Angelic Feathers)"
		},
		{
			type = "checkbox",
			default = true,
			text = ".. Only use in Combat",
			key = "combatfeathers",
			desc = "Only use speed buffs in combat."
		},
		{type = "spacer"},
		{
			type = "checkspin",
			default_check = false,
			default_spin = 3,
			width = 50,
			min = 0,
			max = 100,
			step = 1,
			text = "Void Tendrils",
			key = "tendrils",
			desc = "Automatically use Void Tendrils when set minimum amount of units are close to you."
		}
	}
}